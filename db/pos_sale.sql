-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 03, 2023 at 05:56 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pos_sale`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2023_05_26_145836_create_permission_tables', 1),
(6, '2023_05_26_150238_create_products_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'role-list', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(2, 'role-create', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(3, 'role-edit', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(4, 'role-delete', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(5, 'product-list', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(6, 'product-create', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(7, 'product-edit', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(8, 'product-delete', 'web', '2023-05-26 08:13:22', '2023-05-26 08:13:22'),
(9, 'menu-list', 'web', NULL, NULL),
(10, 'user-list', 'web', NULL, NULL),
(11, 'user-create', 'web', NULL, NULL),
(12, 'user-edit', 'web', NULL, NULL),
(13, 'user-delete', 'web', NULL, NULL),
(14, 'menu-create', 'web', NULL, NULL),
(15, 'menu-edit', 'web', NULL, NULL),
(16, 'menu-delete', 'web', NULL, NULL),
(17, 'home-list', 'web', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `detail`, `created_at`, `updated_at`) VALUES
(1, 'asa', 'saas', NULL, NULL),
(2, 'AdsaDa', 'daDasd', NULL, NULL),
(3, 'asa', 'saas', NULL, NULL),
(4, 'AdsaDa', 'daDasd', NULL, NULL),
(5, 'sdavsacsac', 'sacdcascsac', '2023-05-26 09:09:46', '2023-05-26 09:11:29');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2023-05-26 08:14:11', '2023-05-26 08:14:11'),
(2, 'super admin', 'web', '2023-05-26 09:47:56', '2023-05-26 09:47:56');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 1),
(5, 2),
(6, 1),
(6, 2),
(7, 1),
(7, 2),
(8, 1),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(13, 2),
(14, 2),
(15, 2),
(16, 2),
(17, 1),
(17, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('zF8uRreoecEkfkj8dgBpRVlhz3duFrEjzH3EhoeX', NULL, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoibmZHYVFMUUdFTU1IN2JReU5vQTExdWQzWkg0M081b0FyVEVKOTlOWSI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mzc6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMC9jYXRlZ29yeS1wYWdlLzUiO319', 1684758635),
('SzdSTErAaxKZaWW9vLjL15KIG3ghQGBi3pK7Dmqh', NULL, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoieFB4SGdnZnZMSmFTVTB4a0g4SDQ3WkhRZFU1d3hFSjNBWU05ZlJRUSI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6Mzc6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMC9jYXRlZ29yeS1wYWdlLzUiO31zOjUyOiJsb2dpbl9hZG1pbl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjE7czoxOToicGFzc3dvcmRfaGFzaF9hZG1pbiI7czo2MDoiJDJ5JDEwJFk5bERqU3NjVHpOcjYyWWEucTJvMC5aTlhjYmUzZzRGSVpKOWJZQURhVy91enlVSUdoZU9DIjt9', 1684843427),
('aDNg3cg9FaaGXsJ3mk3WqjJ9iB9hQubDVapNZq7J', 2, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiaFh1MElJN2I3UTFXOHVyeGNyZmphS0t4WTdmeTZhR1JGNEFBRDNQOSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzA6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwMC9wcm9kdWN0cyI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6NTA6ImxvZ2luX3dlYl81OWJhMzZhZGRjMmIyZjk0MDE1ODBmMDE0YzdmNThlYTRlMzA5ODlkIjtpOjI7czo0OiJhdXRoIjthOjE6e3M6MjE6InBhc3N3b3JkX2NvbmZpcm1lZF9hdCI7aToxNjg1NzY0NTQxO319', 1685764552);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_access`
--

CREATE TABLE `tbl_pos_access` (
  `id` int(11) NOT NULL,
  `key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `all_access` tinyint(1) NOT NULL DEFAULT 0,
  `controller` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_additional_item_numbers`
--

CREATE TABLE `tbl_pos_additional_item_numbers` (
  `item_id` int(11) NOT NULL,
  `item_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_variation_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_appointments`
--

CREATE TABLE `tbl_pos_appointments` (
  `id` int(11) NOT NULL,
  `location_id` int(10) NOT NULL,
  `person_id` int(10) DEFAULT NULL,
  `employee_id` int(10) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `appointments_type_id` int(10) NOT NULL,
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_appointment_types`
--

CREATE TABLE `tbl_pos_appointment_types` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_app_files`
--

CREATE TABLE `tbl_pos_app_files` (
  `file_id` int(10) NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_data` longblob NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `expires` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_attributes`
--

CREATE TABLE `tbl_pos_attributes` (
  `id` int(10) NOT NULL,
  `item_id` int(11) DEFAULT NULL,
  `ecommerce_attribute_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `last_modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_attribute_values`
--

CREATE TABLE `tbl_pos_attribute_values` (
  `id` int(10) NOT NULL,
  `ecommerce_attribute_term_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attribute_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `last_modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_categories`
--

CREATE TABLE `tbl_pos_categories` (
  `id` int(11) NOT NULL,
  `ecommerce_category_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` int(1) NOT NULL DEFAULT 0,
  `hide_from_grid` int(1) NOT NULL DEFAULT 0,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_id` int(10) DEFAULT NULL,
  `color` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `system_category` int(1) DEFAULT 0,
  `exclude_from_e_commerce` int(1) NOT NULL DEFAULT 0,
  `category_info_popup` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_currency_exchange_rates`
--

CREATE TABLE `tbl_pos_currency_exchange_rates` (
  `id` int(10) NOT NULL,
  `currency_code_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency_symbol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `exchange_rate` decimal(23,10) NOT NULL,
  `currency_symbol_location` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `number_of_decimals` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `thousands_separator` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `decimal_point` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_customers`
--

CREATE TABLE `tbl_pos_customers` (
  `id` int(10) NOT NULL,
  `person_id` int(10) NOT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `override_default_tax` int(1) NOT NULL DEFAULT 0,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `balance` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `credit_limit` decimal(23,10) DEFAULT NULL,
  `points` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `disable_loyalty` int(1) NOT NULL DEFAULT 0,
  `current_spend_for_points` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `current_sales_for_discount` int(10) NOT NULL DEFAULT 0,
  `taxable` int(1) NOT NULL DEFAULT 1,
  `tax_certificate` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cc_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_expire` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_ref_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_preview` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_issuer` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `tier_id` int(10) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `tax_class_id` int(10) DEFAULT NULL,
  `custom_field_1_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_2_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_3_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_4_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_5_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_6_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_7_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_8_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_9_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_10_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `internal_notes` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_customers_series`
--

CREATE TABLE `tbl_pos_customers_series` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `item_id` int(1) NOT NULL DEFAULT 0,
  `expire_date` date DEFAULT NULL,
  `quantity_remaining` decimal(23,10) DEFAULT 0.0000000000,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_customers_series_log`
--

CREATE TABLE `tbl_pos_customers_series_log` (
  `id` int(11) NOT NULL,
  `series_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `quantity_used` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_customers_taxes`
--

CREATE TABLE `tbl_pos_customers_taxes` (
  `id` int(10) NOT NULL,
  `customer_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_damaged_items_log`
--

CREATE TABLE `tbl_pos_damaged_items_log` (
  `id` int(10) NOT NULL,
  `damaged_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `damaged_qty` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `item_id` int(10) NOT NULL,
  `item_variation_id` int(10) DEFAULT NULL,
  `sale_id` int(10) DEFAULT NULL,
  `location_id` int(10) NOT NULL,
  `damaged_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_ecommerce_locations`
--

CREATE TABLE `tbl_pos_ecommerce_locations` (
  `location_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_pos_ecommerce_locations`
--

INSERT INTO `tbl_pos_ecommerce_locations` (`location_id`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_employees`
--

CREATE TABLE `tbl_pos_employees` (
  `id` int(10) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `force_password_change` int(1) NOT NULL DEFAULT 0,
  `always_require_password` int(1) NOT NULL DEFAULT 0,
  `person_id` int(10) NOT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commission_percent` decimal(23,10) DEFAULT 0.0000000000,
  `commission_percent_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `hourly_pay_rate` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `not_required_to_clock_in` int(1) NOT NULL DEFAULT 0,
  `inactive` int(1) NOT NULL DEFAULT 0,
  `reason_inactive` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `hire_date` date DEFAULT NULL,
  `employee_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `termination_date` date DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `custom_field_1_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_2_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_3_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_4_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_5_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_6_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_7_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_8_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_9_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_10_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `max_discount_percent` decimal(15,3) DEFAULT NULL,
  `login_start_time` time DEFAULT NULL,
  `login_end_time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_pos_employees`
--

INSERT INTO `tbl_pos_employees` (`id`, `username`, `password`, `force_password_change`, `always_require_password`, `person_id`, `language`, `commission_percent`, `commission_percent_type`, `hourly_pay_rate`, `not_required_to_clock_in`, `inactive`, `reason_inactive`, `hire_date`, `employee_number`, `birthday`, `termination_date`, `deleted`, `custom_field_1_value`, `custom_field_2_value`, `custom_field_3_value`, `custom_field_4_value`, `custom_field_5_value`, `custom_field_6_value`, `custom_field_7_value`, `custom_field_8_value`, `custom_field_9_value`, `custom_field_10_value`, `max_discount_percent`, `login_start_time`, `login_end_time`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 0, 0, 1, NULL, '0.0000000000', '', '0.0000000000', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_employees_app_config`
--

CREATE TABLE `tbl_pos_employees_app_config` (
  `employee_id` int(11) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_employees_locations`
--

CREATE TABLE `tbl_pos_employees_locations` (
  `employee_id` int(10) NOT NULL,
  `location_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_pos_employees_locations`
--

INSERT INTO `tbl_pos_employees_locations` (`employee_id`, `location_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_employees_reset_password`
--

CREATE TABLE `tbl_pos_employees_reset_password` (
  `id` int(10) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` int(11) NOT NULL,
  `expire` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_employees_time_clock`
--

CREATE TABLE `tbl_pos_employees_time_clock` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `clock_in` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `clock_out` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `clock_in_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `clock_out_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `hourly_pay_rate` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `ip_address_clock_in` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address_clock_out` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_employees_time_off`
--

CREATE TABLE `tbl_pos_employees_time_off` (
  `id` int(11) NOT NULL,
  `approved` int(1) NOT NULL DEFAULT 0,
  `start_day` date DEFAULT NULL,
  `end_day` date DEFAULT NULL,
  `hours_requested` decimal(23,10) DEFAULT 0.0000000000,
  `is_paid` int(1) NOT NULL DEFAULT 0,
  `reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_requested_person_id` int(10) DEFAULT NULL,
  `employee_requested_location_id` int(10) DEFAULT NULL,
  `employee_approved_person_id` int(10) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_employee_registers`
--

CREATE TABLE `tbl_pos_employee_registers` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `register_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_expenses`
--

CREATE TABLE `tbl_pos_expenses` (
  `id` int(10) NOT NULL,
  `location_id` int(10) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `expense_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expense_description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `expense_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expense_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `expense_amount` decimal(23,10) NOT NULL,
  `expense_tax` decimal(23,10) NOT NULL,
  `expense_note` text COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` int(10) NOT NULL,
  `approved_employee_id` int(10) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `expense_payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_giftcards`
--

CREATE TABLE `tbl_pos_giftcards` (
  `giftcard_id` int(11) NOT NULL,
  `giftcard_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `value` decimal(23,10) NOT NULL,
  `customer_id` int(10) DEFAULT NULL,
  `inactive` int(1) NOT NULL DEFAULT 0,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `integrated_gift_card` int(1) NOT NULL DEFAULT 0,
  `integrated_auth_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_giftcards_log`
--

CREATE TABLE `tbl_pos_giftcards_log` (
  `id` int(10) NOT NULL,
  `log_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `giftcard_id` int(11) NOT NULL,
  `transaction_amount` decimal(23,10) NOT NULL,
  `log_message` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_grid_hidden_categories`
--

CREATE TABLE `tbl_pos_grid_hidden_categories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_grid_hidden_items`
--

CREATE TABLE `tbl_pos_grid_hidden_items` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_grid_hidden_item_kits`
--

CREATE TABLE `tbl_pos_grid_hidden_item_kits` (
  `id` int(11) NOT NULL,
  `item_kit_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_grid_hidden_tags`
--

CREATE TABLE `tbl_pos_grid_hidden_tags` (
  `id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_inventory`
--

CREATE TABLE `tbl_pos_inventory` (
  `trans_id` int(11) NOT NULL,
  `trans_items` int(11) NOT NULL DEFAULT 0,
  `item_variation_id` int(10) DEFAULT NULL,
  `trans_user` int(11) NOT NULL DEFAULT 0,
  `trans_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `trans_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_inventory` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `location_id` int(11) NOT NULL,
  `trans_current_quantity` decimal(23,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_inventory_counts`
--

CREATE TABLE `tbl_pos_inventory_counts` (
  `id` int(11) NOT NULL,
  `count_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `employee_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_inventory_counts_items`
--

CREATE TABLE `tbl_pos_inventory_counts_items` (
  `id` int(11) NOT NULL,
  `inventory_counts_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_variation_id` int(10) DEFAULT NULL,
  `count` decimal(23,10) DEFAULT 0.0000000000,
  `actual_quantity` decimal(23,10) DEFAULT 0.0000000000,
  `comment` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_items`
--

CREATE TABLE `tbl_pos_items` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `item_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ecommerce_product_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ecommerce_product_quantity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_included` int(1) NOT NULL DEFAULT 0,
  `cost_price` decimal(23,10) NOT NULL,
  `unit_price` decimal(23,10) NOT NULL,
  `promo_price` decimal(23,10) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `reorder_level` decimal(23,10) DEFAULT NULL,
  `expire_days` int(10) DEFAULT NULL,
  `item_id` int(10) NOT NULL,
  `allow_alt_description` tinyint(1) NOT NULL,
  `is_serialized` tinyint(1) NOT NULL,
  `override_default_tax` int(1) NOT NULL DEFAULT 0,
  `is_ecommerce` int(1) DEFAULT 1,
  `is_service` int(1) NOT NULL DEFAULT 0,
  `is_ebt_item` int(1) NOT NULL DEFAULT 0,
  `commission_percent` decimal(23,10) DEFAULT 0.0000000000,
  `commission_percent_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `commission_fixed` decimal(23,10) DEFAULT 0.0000000000,
  `change_cost_price` int(1) NOT NULL DEFAULT 0,
  `disable_loyalty` int(1) NOT NULL DEFAULT 0,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `last_modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `ecommerce_last_modified` timestamp NULL DEFAULT NULL,
  `tax_class_id` int(10) DEFAULT NULL,
  `replenish_level` decimal(23,10) DEFAULT NULL,
  `system_item` int(1) NOT NULL DEFAULT 0,
  `max_discount_percent` decimal(15,3) DEFAULT NULL,
  `max_edit_price` decimal(23,10) DEFAULT NULL,
  `min_edit_price` decimal(23,10) DEFAULT NULL,
  `custom_field_1_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_2_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_3_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_4_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_5_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_6_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_7_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_8_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_9_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_10_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `required_age` int(10) DEFAULT NULL,
  `verify_age` int(1) NOT NULL DEFAULT 0,
  `weight` decimal(23,10) DEFAULT NULL,
  `length` decimal(23,10) DEFAULT NULL,
  `width` decimal(23,10) DEFAULT NULL,
  `height` decimal(23,10) DEFAULT NULL,
  `ecommerce_shipping_class_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `long_description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `allow_price_override_regardless_of_permissions` int(1) DEFAULT 0,
  `main_image_id` int(10) DEFAULT NULL,
  `only_integer` int(1) NOT NULL DEFAULT 0,
  `is_series_package` int(1) NOT NULL DEFAULT 0,
  `series_quantity` int(10) DEFAULT NULL,
  `series_days_to_use_within` int(10) DEFAULT NULL,
  `is_barcoded` int(1) NOT NULL DEFAULT 1,
  `default_quantity` decimal(23,10) DEFAULT NULL,
  `disable_from_price_rules` int(1) DEFAULT 0,
  `last_edited` timestamp NULL DEFAULT NULL,
  `info_popup` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_inactive` int(1) DEFAULT 0,
  `barcode_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tags` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `is_favorite` int(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_items_modifiers`
--

CREATE TABLE `tbl_pos_items_modifiers` (
  `item_id` int(10) NOT NULL,
  `modifier_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_items_pricing_history`
--

CREATE TABLE `tbl_pos_items_pricing_history` (
  `id` int(11) NOT NULL,
  `on_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `employee_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_variation_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  `unit_price` decimal(23,10) DEFAULT NULL,
  `cost_price` decimal(23,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_items_quantity_units`
--

CREATE TABLE `tbl_pos_items_quantity_units` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `unit_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit_quantity` decimal(23,10) NOT NULL,
  `unit_price` decimal(23,10) DEFAULT NULL,
  `cost_price` decimal(23,10) DEFAULT NULL,
  `quantity_unit_item_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_items_serial_numbers`
--

CREATE TABLE `tbl_pos_items_serial_numbers` (
  `id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `serial_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_price` decimal(23,10) DEFAULT NULL,
  `cost_price` decimal(23,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_items_tags`
--

CREATE TABLE `tbl_pos_items_tags` (
  `item_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_items_taxes`
--

CREATE TABLE `tbl_pos_items_taxes` (
  `id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_items_tier_prices`
--

CREATE TABLE `tbl_pos_items_tier_prices` (
  `tier_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `unit_price` decimal(23,10) DEFAULT 0.0000000000,
  `percent_off` decimal(15,3) DEFAULT NULL,
  `cost_plus_percent` decimal(15,3) DEFAULT NULL,
  `cost_plus_fixed_amount` decimal(23,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_item_attributes`
--

CREATE TABLE `tbl_pos_item_attributes` (
  `attribute_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_item_attribute_values`
--

CREATE TABLE `tbl_pos_item_attribute_values` (
  `item_id` int(10) NOT NULL,
  `attribute_value_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_item_images`
--

CREATE TABLE `tbl_pos_item_images` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alt_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `item_id` int(11) DEFAULT NULL,
  `item_variation_id` int(10) DEFAULT NULL,
  `ecommerce_image_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_item_kits`
--

CREATE TABLE `tbl_pos_item_kits` (
  `item_kit_id` int(11) NOT NULL,
  `item_kit_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tax_included` int(1) NOT NULL DEFAULT 0,
  `unit_price` decimal(23,10) DEFAULT NULL,
  `cost_price` decimal(23,10) DEFAULT NULL,
  `override_default_tax` int(1) NOT NULL DEFAULT 0,
  `is_ebt_item` int(1) NOT NULL DEFAULT 0,
  `commission_percent` decimal(23,10) DEFAULT 0.0000000000,
  `commission_percent_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `commission_fixed` decimal(23,10) DEFAULT 0.0000000000,
  `change_cost_price` int(1) NOT NULL DEFAULT 0,
  `disable_loyalty` int(1) NOT NULL DEFAULT 0,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `tax_class_id` int(10) DEFAULT NULL,
  `max_discount_percent` decimal(15,3) DEFAULT NULL,
  `max_edit_price` decimal(23,10) DEFAULT NULL,
  `min_edit_price` decimal(23,10) DEFAULT NULL,
  `custom_field_1_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_2_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_3_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_4_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_5_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_6_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_7_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_8_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_9_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_10_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `required_age` int(10) DEFAULT NULL,
  `verify_age` int(1) NOT NULL DEFAULT 0,
  `allow_price_override_regardless_of_permissions` int(1) DEFAULT 0,
  `only_integer` int(1) NOT NULL DEFAULT 0,
  `is_barcoded` int(1) NOT NULL DEFAULT 1,
  `default_quantity` decimal(23,10) DEFAULT NULL,
  `disable_from_price_rules` int(1) DEFAULT 0,
  `main_image_id` int(10) DEFAULT NULL,
  `dynamic_pricing` int(1) NOT NULL DEFAULT 0,
  `info_popup` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `item_kit_inactive` int(1) DEFAULT 0,
  `barcode_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_favorite` int(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_item_kits_modifiers`
--

CREATE TABLE `tbl_pos_item_kits_modifiers` (
  `item_kit_id` int(10) NOT NULL,
  `modifier_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_item_kits_pricing_history`
--

CREATE TABLE `tbl_pos_item_kits_pricing_history` (
  `id` int(11) NOT NULL,
  `on_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `employee_id` int(11) NOT NULL,
  `item_kit_id` int(11) NOT NULL,
  `location_id` int(11) DEFAULT NULL,
  `unit_price` decimal(23,10) DEFAULT NULL,
  `cost_price` decimal(23,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_item_kits_tags`
--

CREATE TABLE `tbl_pos_item_kits_tags` (
  `item_kit_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_item_kits_taxes`
--

CREATE TABLE `tbl_pos_item_kits_taxes` (
  `id` int(10) NOT NULL,
  `item_kit_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_item_kits_tier_prices`
--

CREATE TABLE `tbl_pos_item_kits_tier_prices` (
  `tier_id` int(10) NOT NULL,
  `item_kit_id` int(10) NOT NULL,
  `unit_price` decimal(23,10) DEFAULT 0.0000000000,
  `percent_off` decimal(15,3) DEFAULT NULL,
  `cost_plus_percent` decimal(15,3) DEFAULT NULL,
  `cost_plus_fixed_amount` decimal(23,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_item_kit_images`
--

CREATE TABLE `tbl_pos_item_kit_images` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alt_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `item_kit_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_item_kit_items`
--

CREATE TABLE `tbl_pos_item_kit_items` (
  `id` int(10) NOT NULL,
  `item_kit_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_variation_id` int(10) DEFAULT NULL,
  `quantity` decimal(23,10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_item_kit_item_kits`
--

CREATE TABLE `tbl_pos_item_kit_item_kits` (
  `id` int(10) NOT NULL,
  `item_kit_id` int(11) NOT NULL,
  `item_kit_item_kit` int(11) NOT NULL,
  `quantity` decimal(23,10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_item_variations`
--

CREATE TABLE `tbl_pos_item_variations` (
  `id` int(10) NOT NULL,
  `ecommerce_variation_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ecommerce_variation_quantity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `item_id` int(10) NOT NULL,
  `reorder_level` decimal(23,10) DEFAULT NULL,
  `replenish_level` decimal(23,10) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `item_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_price` decimal(23,10) DEFAULT NULL,
  `cost_price` decimal(23,10) DEFAULT NULL,
  `promo_price` decimal(23,10) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `ecommerce_last_modified` timestamp NULL DEFAULT NULL,
  `is_ecommerce` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_item_variation_attribute_values`
--

CREATE TABLE `tbl_pos_item_variation_attribute_values` (
  `attribute_value_id` int(10) NOT NULL,
  `item_variation_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_keys`
--

CREATE TABLE `tbl_pos_keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `key_ending` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT 0,
  `is_private_key` tinyint(1) NOT NULL DEFAULT 0,
  `ip_addresses` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_limits`
--

CREATE TABLE `tbl_pos_limits` (
  `id` int(11) NOT NULL,
  `api_key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(10) NOT NULL,
  `hour_started` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_locations`
--

CREATE TABLE `tbl_pos_locations` (
  `location_id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_logo` int(10) DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_email` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `bcc_email` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `return_policy` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `receive_stock_alert` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `stock_alert_email` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `mailchimp_api_key` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `enable_credit_card_processing` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `credit_card_processor` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `hosted_checkout_merchant_id` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `hosted_checkout_merchant_password` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `emv_merchant_id` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `net_e_pay_server` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `listener_port` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `com_port` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_public` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_private` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_currency_code` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `braintree_merchant_id` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `braintree_public_key` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `braintree_private_key` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_tax_1_rate` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_tax_1_name` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_tax_2_rate` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_tax_2_name` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_tax_2_cumulative` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_tax_3_rate` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_tax_3_name` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_tax_4_rate` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_tax_4_name` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_tax_5_rate` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_tax_5_name` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) DEFAULT 0,
  `secure_device_override_emv` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `secure_device_override_non_emv` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `tax_class_id` int(10) DEFAULT NULL,
  `ebt_integrated` int(1) NOT NULL DEFAULT 0,
  `integrated_gift_cards` int(1) NOT NULL DEFAULT 0,
  `square_currency_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'USD',
  `square_location_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `square_currency_multiplier` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '100',
  `email_sales_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_receivings_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stock_alerts_just_order_level` int(1) DEFAULT 0,
  `platformly_api_key` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `platformly_project_id` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `disable_markup_markdown` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_connect_mid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_connect_rest_username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_connect_rest_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_pos_locations`
--

INSERT INTO `tbl_pos_locations` (`location_id`, `name`, `company`, `website`, `company_logo`, `address`, `phone`, `fax`, `email`, `cc_email`, `bcc_email`, `color`, `return_policy`, `receive_stock_alert`, `stock_alert_email`, `timezone`, `mailchimp_api_key`, `enable_credit_card_processing`, `credit_card_processor`, `hosted_checkout_merchant_id`, `hosted_checkout_merchant_password`, `emv_merchant_id`, `net_e_pay_server`, `listener_port`, `com_port`, `stripe_public`, `stripe_private`, `stripe_currency_code`, `braintree_merchant_id`, `braintree_public_key`, `braintree_private_key`, `default_tax_1_rate`, `default_tax_1_name`, `default_tax_2_rate`, `default_tax_2_name`, `default_tax_2_cumulative`, `default_tax_3_rate`, `default_tax_3_name`, `default_tax_4_rate`, `default_tax_4_name`, `default_tax_5_rate`, `default_tax_5_name`, `deleted`, `secure_device_override_emv`, `secure_device_override_non_emv`, `tax_class_id`, `ebt_integrated`, `integrated_gift_cards`, `square_currency_code`, `square_location_id`, `square_currency_multiplier`, `email_sales_email`, `email_receivings_email`, `stock_alerts_just_order_level`, `platformly_api_key`, `platformly_project_id`, `tax_id`, `disable_markup_markdown`, `card_connect_mid`, `card_connect_rest_username`, `card_connect_rest_password`) VALUES
(1, 'Default', NULL, NULL, NULL, '123 Nowhere street', '015338826', '', 'info@poscambodia.com', NULL, NULL, NULL, NULL, '0', '', 'Asia/Phnom_Penh', '', '0', NULL, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '0', '', '', '', '', '', '', 0, '', '', NULL, 1, 0, 'USD', '', '100', NULL, NULL, 0, NULL, NULL, '', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_location_items`
--

CREATE TABLE `tbl_pos_location_items` (
  `location_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cost_price` decimal(23,10) DEFAULT NULL,
  `unit_price` decimal(23,10) DEFAULT NULL,
  `promo_price` decimal(23,10) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `quantity` decimal(23,10) DEFAULT 0.0000000000,
  `reorder_level` decimal(23,10) DEFAULT NULL,
  `override_default_tax` int(1) NOT NULL DEFAULT 0,
  `tax_class_id` int(10) DEFAULT NULL,
  `replenish_level` decimal(23,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_location_items_taxes`
--

CREATE TABLE `tbl_pos_location_items_taxes` (
  `id` int(10) NOT NULL,
  `location_id` int(11) NOT NULL,
  `item_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(16,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_location_items_tier_prices`
--

CREATE TABLE `tbl_pos_location_items_tier_prices` (
  `tier_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `location_id` int(10) NOT NULL,
  `unit_price` decimal(23,10) DEFAULT 0.0000000000,
  `percent_off` decimal(15,3) DEFAULT NULL,
  `cost_plus_percent` decimal(15,3) DEFAULT NULL,
  `cost_plus_fixed_amount` decimal(23,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_location_item_kits`
--

CREATE TABLE `tbl_pos_location_item_kits` (
  `location_id` int(11) NOT NULL,
  `item_kit_id` int(11) NOT NULL,
  `unit_price` decimal(23,10) DEFAULT NULL,
  `cost_price` decimal(23,10) DEFAULT NULL,
  `override_default_tax` int(1) NOT NULL DEFAULT 0,
  `tax_class_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_location_item_kits_taxes`
--

CREATE TABLE `tbl_pos_location_item_kits_taxes` (
  `id` int(10) NOT NULL,
  `location_id` int(11) NOT NULL,
  `item_kit_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(16,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_location_item_kits_tier_prices`
--

CREATE TABLE `tbl_pos_location_item_kits_tier_prices` (
  `tier_id` int(10) NOT NULL,
  `item_kit_id` int(10) NOT NULL,
  `location_id` int(10) NOT NULL,
  `unit_price` decimal(23,10) DEFAULT 0.0000000000,
  `percent_off` decimal(15,3) DEFAULT NULL,
  `cost_plus_percent` decimal(15,3) DEFAULT NULL,
  `cost_plus_fixed_amount` decimal(23,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_location_item_variations`
--

CREATE TABLE `tbl_pos_location_item_variations` (
  `item_variation_id` int(10) NOT NULL,
  `location_id` int(10) NOT NULL,
  `quantity` int(1) DEFAULT NULL,
  `reorder_level` decimal(23,10) DEFAULT NULL,
  `replenish_level` decimal(23,10) DEFAULT NULL,
  `unit_price` decimal(23,10) DEFAULT NULL,
  `cost_price` decimal(23,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_logs`
--

CREATE TABLE `tbl_pos_logs` (
  `id` int(11) NOT NULL,
  `api_key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `method` enum('get','post','options','put','patch','delete') COLLATE utf8_unicode_ci NOT NULL,
  `params` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `response_code` smallint(3) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_manufacturers`
--

CREATE TABLE `tbl_pos_manufacturers` (
  `id` int(11) NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_messages`
--

CREATE TABLE `tbl_pos_messages` (
  `id` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `sender_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_message_receiver`
--

CREATE TABLE `tbl_pos_message_receiver` (
  `id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message_read` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_migrations`
--

CREATE TABLE `tbl_pos_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_pos_migrations`
--

INSERT INTO `tbl_pos_migrations` (`version`) VALUES
(20200521135727);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_modifiers`
--

CREATE TABLE `tbl_pos_modifiers` (
  `id` int(10) NOT NULL,
  `sort_order` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_modifier_items`
--

CREATE TABLE `tbl_pos_modifier_items` (
  `id` int(10) NOT NULL,
  `sort_order` int(10) NOT NULL DEFAULT 0,
  `modifier_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cost_price` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `unit_price` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_modules`
--

CREATE TABLE `tbl_pos_modules` (
  `name_lang_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `desc_lang_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(10) NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `module_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_pos_modules`
--

INSERT INTO `tbl_pos_modules` (`name_lang_key`, `desc_lang_key`, `sort`, `icon`, `module_id`) VALUES
('module_appointments', 'module_appointments_desc', 75, 'ti-calendar', 'appointments'),
('module_config', 'module_config_desc', 100, 'icon ti-settings', 'config'),
('module_customers', 'module_customers_desc', 10, 'icon ti-user', 'customers'),
('module_deliveries', 'module_deliveries_desc', 71, 'ion-android-car', 'deliveries'),
('module_employees', 'module_employees_desc', 80, 'icon ti-id-badge', 'employees'),
('module_expenses', 'module_expenses_desc', 75, 'icon ti-money', 'expenses'),
('module_giftcards', 'module_giftcards_desc', 90, 'icon ti-credit-card', 'giftcards'),
('module_item_kits', 'module_item_kits_desc', 30, 'icon ti-harddrives', 'item_kits'),
('module_items', 'module_items_desc', 20, 'icon ti-harddrive', 'items'),
('module_locations', 'module_locations_desc', 110, 'icon ti-home', 'locations'),
('module_messages', 'module_messages_desc', 120, 'icon ti-email', 'messages'),
('module_price_rules', 'module_item_price_rules_desc', 35, 'ion-ios-pricetags-outline', 'price_rules'),
('module_receivings', 'module_receivings_desc', 60, 'icon ti-cloud-down', 'receivings'),
('module_reports', 'module_reports_desc', 50, 'icon ti-bar-chart', 'reports'),
('module_sales', 'module_sales_desc', 70, 'icon ti-shopping-cart', 'sales'),
('module_suppliers', 'module_suppliers_desc', 40, 'icon ti-download', 'suppliers');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_modules_actions`
--

CREATE TABLE `tbl_pos_modules_actions` (
  `action_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `module_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `action_name_key` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_pos_modules_actions`
--

INSERT INTO `tbl_pos_modules_actions` (`action_id`, `module_id`, `action_name_key`, `sort`) VALUES
('add', 'appointments', 'appointments_add', 240),
('add_update', 'customers', 'module_action_add_update', 1),
('add_update', 'deliveries', 'deliveries_add_update', 240),
('add_update', 'employees', 'module_action_add_update', 130),
('add_update', 'expenses', 'module_expenses_add_update', 315),
('add_update', 'giftcards', 'module_action_add_update', 200),
('add_update', 'item_kits', 'module_action_add_update', 70),
('add_update', 'items', 'module_action_add_update', 40),
('add_update', 'locations', 'module_action_add_update', 240),
('add_update', 'price_rules', 'module_action_add_update', 400),
('add_update', 'suppliers', 'module_action_add_update', 100),
('allow_customer_search_suggestions_for_sales', 'sales', 'sales_allow_customer_search_suggestions_for_sales', 302),
('allow_item_search_suggestions_for_receivings', 'receivings', 'receivings_allow_item_search_suggestions_for_receivings', 301),
('allow_item_search_suggestions_for_sales', 'sales', 'sales_allow_item_search_suggestions_for_sales', 300),
('allow_supplier_search_suggestions_for_suppliers', 'receivings', 'receivings_allow_supplier_search_suggestions_for_suppliers', 303),
('assign_all_locations', 'employees', 'module_action_assign_all_locations', 151),
('can_edit_inventory_comment', 'items', 'items_can_edit_inventory_comment', 500),
('count_inventory', 'items', 'items_count_inventory', 65),
('delete', 'appointments', 'appointments_delete', 250),
('delete', 'customers', 'module_action_delete', 20),
('delete', 'deliveries', 'deliveries_delete', 250),
('delete', 'employees', 'module_action_delete', 140),
('delete', 'expenses', 'module_expenses_delete', 330),
('delete', 'giftcards', 'module_action_delete', 210),
('delete', 'item_kits', 'module_action_delete', 80),
('delete', 'items', 'module_action_delete', 50),
('delete', 'locations', 'module_action_delete', 250),
('delete', 'price_rules', 'module_action_delete', 405),
('delete', 'suppliers', 'module_action_delete', 110),
('delete_receiving', 'receivings', 'module_action_delete_receiving', 306),
('delete_register_log', 'reports', 'common_delete_register_log', 232),
('delete_sale', 'sales', 'module_action_delete_sale', 230),
('delete_suspended_sale', 'sales', 'module_action_delete_suspended_sale', 181),
('delete_taxes', 'receivings', 'module_action_delete_taxes', 300),
('delete_taxes', 'sales', 'module_action_delete_taxes', 182),
('edit', 'appointments', 'appointments_edit', 245),
('edit', 'deliveries', 'deliveries_edit', 245),
('edit_customer_points', 'customers', 'module_edit_customer_points', 35),
('edit_giftcard_value', 'giftcards', 'module_edit_giftcard_value', 205),
('edit_prices', 'item_kits', 'common_edit_prices', 502),
('edit_prices', 'items', 'common_edit_prices', 501),
('edit_profile', 'employees', 'common_edit_profile', 155),
('edit_quantity', 'items', 'items_edit_quantity', 62),
('edit_receiving', 'receivings', 'module_action_edit_receiving', 303),
('edit_register_log', 'reports', 'common_edit_register_log', 231),
('edit_sale', 'sales', 'module_edit_sale', 190),
('edit_sale_cost_price', 'sales', 'module_edit_sale_cost_price', 175),
('edit_sale_price', 'sales', 'module_edit_sale_price', 170),
('edit_store_account_balance', 'customers', 'customers_edit_store_account_balance', 31),
('edit_store_account_balance', 'suppliers', 'suppliers_edit_store_account_balance', 130),
('edit_suspended_sale', 'sales', 'sales_edit_suspended_sale', 192),
('edit_taxes', 'receivings', 'module_edit_taxes', 304),
('edit_taxes', 'sales', 'module_edit_taxes', 191),
('edit_tier', 'customers', 'customers_edit_tier', 45),
('excel_export', 'customers', 'common_excel_export', 40),
('excel_export', 'employees', 'common_excel_export', 160),
('excel_export', 'giftcards', 'common_excel_export', 225),
('excel_export', 'item_kits', 'common_excel_export', 95),
('excel_export', 'items', 'common_excel_export', 80),
('excel_export', 'suppliers', 'common_excel_export', 135),
('give_discount', 'sales', 'module_give_discount', 180),
('manage_categories', 'items', 'items_manage_categories', 70),
('manage_manufacturers', 'items', 'items_manage_manufacturers', 76),
('manage_tags', 'items', 'items_manage_tags', 75),
('process_returns', 'sales', 'config_process_returns', 184),
('receive_store_account_payment', 'receivings', 'common_receive_store_account_payment', 260),
('receive_store_account_payment', 'sales', 'common_receive_store_account_payment', 255),
('search', 'appointments', 'appointments_search', 255),
('search', 'customers', 'module_action_search_customers', 30),
('search', 'deliveries', 'deliveries_search', 255),
('search', 'employees', 'module_action_search_employees', 150),
('search', 'expenses', 'module_expenses_search', 310),
('search', 'giftcards', 'module_action_search_giftcards', 220),
('search', 'item_kits', 'module_action_search_item_kits', 90),
('search', 'items', 'module_action_search_items', 60),
('search', 'locations', 'module_action_search_locations', 260),
('search', 'price_rules', 'module_action_search_price_rules', 415),
('search', 'sales', 'module_action_search_sales', 235),
('search', 'suppliers', 'module_action_search_suppliers', 120),
('see_cost_price', 'item_kits', 'module_see_cost_price', 91),
('see_cost_price', 'items', 'module_see_cost_price', 61),
('see_count_when_count_inventory', 'items', 'items_see_count_when_count_inventory', 66),
('send_message', 'messages', 'employees_send_message', 350),
('show_cost_price', 'reports', 'reports_show_cost_price', 290),
('show_profit', 'reports', 'reports_show_profit', 280),
('suspend_sale', 'sales', 'sales_suspend_sale', 183),
('view_all_employee_commissions', 'reports', 'reports_view_all_employee_commissions', 107),
('view_appointments', 'reports', 'reports_appointments', 95),
('view_categories', 'reports', 'reports_categories', 100),
('view_closeout', 'reports', 'reports_closeout', 105),
('view_commissions', 'reports', 'reports_commission', 106),
('view_customers', 'reports', 'reports_customers', 120),
('view_dashboard_stats', 'reports', 'reports_view_dashboard_stats', 300),
('view_deleted_sales', 'reports', 'reports_deleted_sales', 130),
('view_deliveries', 'reports', 'reports_deliveries', 135),
('view_discounts', 'reports', 'reports_discounts', 140),
('view_employees', 'reports', 'reports_employees', 150),
('view_expenses', 'reports', 'module_expenses_report', 155),
('view_giftcards', 'reports', 'reports_giftcards', 160),
('view_inventory_at_all_locations', 'items', 'common_view_inventory_at_all_locations', 268),
('view_inventory_at_all_locations', 'reports', 'reports_view_inventory_at_all_locations', 300),
('view_inventory_print_list', 'items', 'common_view_inventory_print_list', 267),
('view_inventory_reports', 'reports', 'reports_inventory_reports', 170),
('view_item_kits', 'reports', 'module_item_kits', 180),
('view_items', 'reports', 'reports_items', 190),
('view_manufacturers', 'reports', 'reports_manufacturers', 195),
('view_payments', 'reports', 'reports_payments', 200),
('view_price_rules', 'reports', 'reports_price_rules', 205),
('view_profit_and_loss', 'reports', 'reports_profit_and_loss', 210),
('view_receivings', 'reports', 'reports_receivings', 220),
('view_register_log', 'reports', 'reports_register_log_title', 230),
('view_registers', 'reports', 'reports_registers', 235),
('view_sales', 'reports', 'reports_sales', 240),
('view_sales_generator', 'reports', 'reports_sales_generator', 110),
('view_store_account', 'reports', 'reports_store_account', 250),
('view_store_account_suppliers', 'reports', 'reports_store_account_suppliers', 255),
('view_suppliers', 'reports', 'reports_suppliers', 260),
('view_suspended_sales', 'reports', 'reports_suspended_sales', 261),
('view_tags', 'reports', 'common_tags', 264),
('view_taxes', 'reports', 'reports_taxes', 270),
('view_tiers', 'reports', 'reports_tiers', 275),
('view_timeclock', 'reports', 'employees_timeclock', 280);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_people`
--

CREATE TABLE `tbl_pos_people` (
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` text COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comments` text COLLATE utf8_unicode_ci NOT NULL,
  `image_id` int(10) DEFAULT NULL,
  `person_id` int(10) NOT NULL,
  `create_date` timestamp NULL DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_pos_people`
--

INSERT INTO `tbl_pos_people` (`first_name`, `last_name`, `full_name`, `phone_number`, `email`, `address_1`, `address_2`, `city`, `state`, `zip`, `country`, `comments`, `image_id`, `person_id`, `create_date`, `last_modified`) VALUES
('POSCAMBODIA', 'POS', 'CAMBODIA', '015338826', 'info@poscambodia.com', 'Address 1', '', '', '', '', '', '', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_people_files`
--

CREATE TABLE `tbl_pos_people_files` (
  `id` int(11) NOT NULL,
  `file_id` int(10) NOT NULL,
  `person_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_permissions_actions_locations`
--

CREATE TABLE `tbl_pos_permissions_actions_locations` (
  `module_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int(11) NOT NULL,
  `action_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `location_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_permissions_locations`
--

CREATE TABLE `tbl_pos_permissions_locations` (
  `module_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `person_id` int(10) NOT NULL,
  `location_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_price_rules`
--

CREATE TABLE `tbl_pos_price_rules` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `added_on` timestamp NULL DEFAULT current_timestamp(),
  `active` int(1) NOT NULL DEFAULT 1,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `items_to_buy` decimal(23,10) DEFAULT NULL,
  `items_to_get` decimal(23,10) DEFAULT NULL,
  `percent_off` decimal(23,10) DEFAULT NULL,
  `fixed_off` decimal(23,10) DEFAULT NULL,
  `spend_amount` decimal(23,10) DEFAULT NULL,
  `num_times_to_apply` int(10) NOT NULL,
  `coupon_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `show_on_receipt` int(1) NOT NULL DEFAULT 0,
  `coupon_spend_amount` decimal(23,10) DEFAULT NULL,
  `mix_and_match` int(1) NOT NULL DEFAULT 0,
  `disable_loyalty_for_rule` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_price_rules_categories`
--

CREATE TABLE `tbl_pos_price_rules_categories` (
  `id` int(10) NOT NULL,
  `rule_id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_price_rules_items`
--

CREATE TABLE `tbl_pos_price_rules_items` (
  `id` int(10) NOT NULL,
  `rule_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_price_rules_item_kits`
--

CREATE TABLE `tbl_pos_price_rules_item_kits` (
  `id` int(10) NOT NULL,
  `rule_id` int(10) NOT NULL,
  `item_kit_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_price_rules_locations`
--

CREATE TABLE `tbl_pos_price_rules_locations` (
  `id` int(10) NOT NULL,
  `rule_id` int(10) NOT NULL,
  `location_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_price_rules_manufacturers`
--

CREATE TABLE `tbl_pos_price_rules_manufacturers` (
  `id` int(10) NOT NULL,
  `rule_id` int(10) NOT NULL,
  `manufacturer_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_price_rules_price_breaks`
--

CREATE TABLE `tbl_pos_price_rules_price_breaks` (
  `id` int(10) NOT NULL,
  `rule_id` int(10) NOT NULL,
  `item_qty_to_buy` decimal(23,10) DEFAULT NULL,
  `discount_per_unit_fixed` decimal(23,10) DEFAULT NULL,
  `discount_per_unit_percent` decimal(23,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_price_rules_tags`
--

CREATE TABLE `tbl_pos_price_rules_tags` (
  `id` int(10) NOT NULL,
  `rule_id` int(10) NOT NULL,
  `tag_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_price_tiers`
--

CREATE TABLE `tbl_pos_price_tiers` (
  `id` int(10) NOT NULL,
  `order` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default_percent_off` decimal(15,3) DEFAULT NULL,
  `default_cost_plus_percent` decimal(15,3) DEFAULT NULL,
  `default_cost_plus_fixed_amount` decimal(23,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_receivings`
--

CREATE TABLE `tbl_pos_receivings` (
  `receiving_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `supplier_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT 0,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `receiving_id` int(10) NOT NULL,
  `payment_type` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `deleted_by` int(10) DEFAULT NULL,
  `suspended` int(1) NOT NULL DEFAULT 0,
  `location_id` int(11) NOT NULL,
  `transfer_to_location_id` int(11) DEFAULT NULL,
  `deleted_taxes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_po` int(1) NOT NULL DEFAULT 0,
  `store_account_payment` int(1) NOT NULL DEFAULT 0,
  `total_quantity_purchased` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `total_quantity_received` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `subtotal` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `tax` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `total` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `profit` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `custom_field_1_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_2_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_3_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_4_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_5_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_6_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_7_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_8_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_9_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_10_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `override_taxes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_cost` decimal(23,10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_receivings_items`
--

CREATE TABLE `tbl_pos_receivings_items` (
  `receiving_id` int(10) NOT NULL DEFAULT 0,
  `item_id` int(10) NOT NULL DEFAULT 0,
  `item_variation_id` int(10) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serialnumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(11) NOT NULL DEFAULT 0,
  `quantity_purchased` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `quantity_received` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `item_cost_price` decimal(23,10) NOT NULL,
  `item_unit_price` decimal(23,10) NOT NULL,
  `discount_percent` decimal(15,3) NOT NULL DEFAULT 0.000,
  `expire_date` date DEFAULT NULL,
  `subtotal` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `tax` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `total` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `profit` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `override_taxes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_quantity` decimal(23,10) DEFAULT NULL,
  `items_quantity_units_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_receivings_items_taxes`
--

CREATE TABLE `tbl_pos_receivings_items_taxes` (
  `receiving_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_receivings_payments`
--

CREATE TABLE `tbl_pos_receivings_payments` (
  `payment_id` int(10) NOT NULL,
  `receiving_id` int(10) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_amount` decimal(23,10) NOT NULL,
  `payment_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_registers`
--

CREATE TABLE `tbl_pos_registers` (
  `register_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `iptran_device_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emv_terminal_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `card_connect_hsn` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_pos_registers`
--

INSERT INTO `tbl_pos_registers` (`register_id`, `location_id`, `name`, `iptran_device_id`, `emv_terminal_id`, `deleted`, `card_connect_hsn`) VALUES
(1, 1, 'Default', NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_registers_cart`
--

CREATE TABLE `tbl_pos_registers_cart` (
  `id` int(10) NOT NULL,
  `register_id` int(11) NOT NULL,
  `data` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_register_currency_denominations`
--

CREATE TABLE `tbl_pos_register_currency_denominations` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` decimal(23,10) NOT NULL,
  `deleted` int(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_pos_register_currency_denominations`
--

INSERT INTO `tbl_pos_register_currency_denominations` (`id`, `name`, `value`, `deleted`) VALUES
(1, '100\'s', '100.0000000000', 0),
(2, '50\'s', '50.0000000000', 0),
(3, '20\'s', '20.0000000000', 0),
(4, '10\'s', '10.0000000000', 0),
(5, '5\'s', '5.0000000000', 0),
(6, '2\'s', '2.0000000000', 0),
(7, '1\'s', '1.0000000000', 0),
(8, 'Half Dollars', '0.5000000000', 0),
(9, 'Quarters', '0.2500000000', 0),
(10, 'Dimes', '0.1000000000', 0),
(11, 'Nickels', '0.0500000000', 0),
(12, 'Pennies', '0.0100000000', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_register_log`
--

CREATE TABLE `tbl_pos_register_log` (
  `register_log_id` int(10) NOT NULL,
  `employee_id_open` int(10) NOT NULL,
  `employee_id_close` int(11) DEFAULT NULL,
  `register_id` int(11) DEFAULT NULL,
  `shift_start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shift_end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `notes` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_register_log_audit`
--

CREATE TABLE `tbl_pos_register_log_audit` (
  `id` int(11) NOT NULL,
  `register_log_id` int(10) NOT NULL,
  `employee_id` int(10) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `amount` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_register_log_denoms`
--

CREATE TABLE `tbl_pos_register_log_denoms` (
  `id` int(11) NOT NULL,
  `register_log_id` int(11) NOT NULL,
  `register_currency_denominations_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_register_log_payments`
--

CREATE TABLE `tbl_pos_register_log_payments` (
  `id` int(11) NOT NULL,
  `register_log_id` int(10) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `open_amount` decimal(23,10) NOT NULL,
  `close_amount` decimal(23,10) NOT NULL,
  `payment_sales_amount` decimal(23,10) NOT NULL,
  `total_payment_additions` decimal(23,10) NOT NULL,
  `total_payment_subtractions` decimal(23,10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_sales`
--

CREATE TABLE `tbl_pos_sales` (
  `sale_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `customer_id` int(10) DEFAULT NULL,
  `employee_id` int(10) NOT NULL DEFAULT 0,
  `sold_by_employee_id` int(10) DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `discount_reason` text COLLATE utf8_unicode_ci NOT NULL,
  `show_comment_on_receipt` int(1) NOT NULL DEFAULT 0,
  `sale_id` int(10) NOT NULL,
  `rule_id` int(10) DEFAULT NULL,
  `rule_discount` decimal(23,10) DEFAULT NULL,
  `payment_type` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_ref_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `deleted_by` int(10) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `suspended` int(10) NOT NULL DEFAULT 0,
  `is_ecommerce` int(1) NOT NULL DEFAULT 0,
  `ecommerce_order_id` int(10) DEFAULT NULL,
  `ecommerce_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `store_account_payment` int(1) NOT NULL DEFAULT 0,
  `was_layaway` int(1) NOT NULL DEFAULT 0,
  `was_estimate` int(1) NOT NULL DEFAULT 0,
  `location_id` int(11) NOT NULL,
  `register_id` int(11) DEFAULT NULL,
  `tier_id` int(10) DEFAULT NULL,
  `points_used` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `points_gained` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `did_redeem_discount` int(1) NOT NULL DEFAULT 0,
  `signature_image_id` int(10) DEFAULT NULL,
  `deleted_taxes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_quantity_purchased` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `subtotal` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `tax` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `total` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `profit` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `exchange_rate` decimal(23,10) NOT NULL DEFAULT 1.0000000000,
  `exchange_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exchange_currency_symbol` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exchange_currency_symbol_location` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exchange_number_of_decimals` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exchange_thousands_separator` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `exchange_decimal_point` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `is_purchase_points` int(1) NOT NULL DEFAULT 0,
  `custom_field_1_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_2_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_3_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_4_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_5_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_6_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_7_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_8_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_9_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_10_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_modified` timestamp NULL DEFAULT NULL,
  `override_taxes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `return_sale_id` int(10) DEFAULT NULL,
  `tip` decimal(23,10) DEFAULT NULL,
  `total_quantity_received` decimal(23,10) NOT NULL DEFAULT 0.0000000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_sales_coupons`
--

CREATE TABLE `tbl_pos_sales_coupons` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_sales_deliveries`
--

CREATE TABLE `tbl_pos_sales_deliveries` (
  `id` int(11) NOT NULL,
  `sale_id` int(10) NOT NULL,
  `shipping_address_person_id` int(10) NOT NULL,
  `delivery_employee_person_id` int(10) DEFAULT NULL,
  `shipping_method_id` int(10) DEFAULT NULL,
  `shipping_zone_id` int(10) DEFAULT NULL,
  `tax_class_id` int(10) DEFAULT NULL,
  `status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `estimated_shipping_date` timestamp NULL DEFAULT NULL,
  `actual_shipping_date` timestamp NULL DEFAULT NULL,
  `estimated_delivery_or_pickup_date` timestamp NULL DEFAULT NULL,
  `actual_delivery_or_pickup_date` timestamp NULL DEFAULT NULL,
  `is_pickup` int(1) NOT NULL DEFAULT 0,
  `tracking_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `deleted` int(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_sales_items`
--

CREATE TABLE `tbl_pos_sales_items` (
  `sale_id` int(10) NOT NULL DEFAULT 0,
  `item_id` int(10) NOT NULL DEFAULT 0,
  `item_variation_id` int(10) DEFAULT NULL,
  `rule_id` int(10) DEFAULT NULL,
  `rule_discount` decimal(23,10) DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `serialnumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(11) NOT NULL DEFAULT 0,
  `quantity_purchased` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `quantity_received` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `item_cost_price` decimal(23,10) NOT NULL,
  `item_unit_price` decimal(23,10) NOT NULL,
  `regular_item_unit_price_at_time_of_sale` decimal(23,10) DEFAULT NULL,
  `discount_percent` decimal(15,3) NOT NULL DEFAULT 0.000,
  `commission` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `subtotal` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `tax` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `total` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `profit` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `tier_id` int(10) DEFAULT NULL,
  `series_id` int(11) DEFAULT NULL,
  `damaged_qty` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `override_taxes` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `unit_quantity` decimal(23,10) DEFAULT NULL,
  `items_quantity_units_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_sales_items_modifier_items`
--

CREATE TABLE `tbl_pos_sales_items_modifier_items` (
  `item_id` int(10) NOT NULL,
  `sale_id` int(10) NOT NULL,
  `line` int(10) NOT NULL,
  `modifier_item_id` int(10) NOT NULL,
  `cost_price` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `unit_price` decimal(23,10) NOT NULL DEFAULT 0.0000000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_sales_items_taxes`
--

CREATE TABLE `tbl_pos_sales_items_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_id` int(10) NOT NULL,
  `line` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_sales_item_kits`
--

CREATE TABLE `tbl_pos_sales_item_kits` (
  `sale_id` int(10) NOT NULL DEFAULT 0,
  `item_kit_id` int(10) NOT NULL DEFAULT 0,
  `rule_id` int(10) DEFAULT NULL,
  `rule_discount` decimal(23,10) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `line` int(11) NOT NULL DEFAULT 0,
  `quantity_purchased` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `quantity_received` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `item_kit_cost_price` decimal(23,10) NOT NULL,
  `item_kit_unit_price` decimal(23,10) NOT NULL,
  `regular_item_kit_unit_price_at_time_of_sale` decimal(23,10) DEFAULT NULL,
  `discount_percent` decimal(15,3) NOT NULL DEFAULT 0.000,
  `commission` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `subtotal` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `tax` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `total` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `profit` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `tier_id` int(10) DEFAULT NULL,
  `override_taxes` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_sales_item_kits_modifier_items`
--

CREATE TABLE `tbl_pos_sales_item_kits_modifier_items` (
  `item_kit_id` int(10) NOT NULL,
  `sale_id` int(10) NOT NULL,
  `line` int(10) NOT NULL,
  `modifier_item_id` int(10) NOT NULL,
  `cost_price` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `unit_price` decimal(23,10) NOT NULL DEFAULT 0.0000000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_sales_item_kits_taxes`
--

CREATE TABLE `tbl_pos_sales_item_kits_taxes` (
  `sale_id` int(10) NOT NULL,
  `item_kit_id` int(10) NOT NULL,
  `line` int(11) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_sales_payments`
--

CREATE TABLE `tbl_pos_sales_payments` (
  `payment_id` int(10) NOT NULL,
  `sale_id` int(10) NOT NULL,
  `payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_amount` decimal(23,10) NOT NULL,
  `auth_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `ref_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `cc_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `acq_ref_data` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `process_data` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `entry_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `aid` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `tvr` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `iad` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `tsi` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `arc` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `cvm` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `tran_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `application_label` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `truncated_card` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `card_issuer` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `payment_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `ebt_auth_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `ebt_voucher_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_sale_types`
--

CREATE TABLE `tbl_pos_sale_types` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort` int(10) NOT NULL,
  `system_sale_type` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_pos_sale_types`
--

INSERT INTO `tbl_pos_sale_types` (`id`, `name`, `sort`, `system_sale_type`) VALUES
(0, 'common_sale', 0, 1),
(1, 'common_layaway', 0, 1),
(2, 'common_estimate', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_sessions`
--

CREATE TABLE `tbl_pos_sessions` (
  `id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_pos_sessions`
--

INSERT INTO `tbl_pos_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('4226f6a4ba3cea4ed4b9a6519d82138ea6c8647d', '::1', 1683808781, 0x706572736f6e5f69647c733a313a2231223b726563656976696e677c733a313230353a224f3a31343a22504850504f534361727452656376223a34313a7b733a31323a22726563656976696e675f6964223b4e3b733a31313a22737570706c6965725f6964223b4e3b733a32303a227472616e736665725f6c6f636174696f6e5f6964223b4e3b733a32353a227472616e736665725f66726f6d5f6c6f636174696f6e5f6964223b4e3b733a353a2269735f706f223b623a303b733a31333a227368697070696e675f636f7374223b4e3b733a31393a2200504850504f534361727400636172745f6964223b733a393a22726563656976696e67223b733a32323a2200504850504f534361727400636172745f6974656d73223b613a303a7b7d733a32303a2200504850504f5343617274007061796d656e7473223b613a303a7b7d733a32363a2200504850504f5343617274006578636c756465645f7461786573223b613a303a7b7d733a31363a2200504850504f5343617274006d6f6465223b733a373a2272656365697665223b733a33343a2200504850504f534361727400706169645f73746f72655f6163636f756e745f696473223b613a303a7b7d733a32363a22706169645f73746f72655f6163636f756e745f616d6f756e7473223b4e3b733a31313a22656d706c6f7965655f6964223b4e3b733a31313a226c6f636174696f6e5f6964223b4e3b733a31313a2272656769737465725f6964223b4e3b733a373a22636f6d6d656e74223b733a303a22223b733a31363a2273656c65637465645f7061796d656e74223b733a303a22223b733a31333a22656d61696c5f72656365697074223b623a303b733a393a2273757370656e646564223b693a303b733a31383a226368616e67655f646174655f656e61626c65223b623a303b733a31363a226368616e67655f636172745f64617465223b4e3b733a31393a2269735f65646974696e675f70726576696f7573223b623a303b733a32303a22637573746f6d5f6669656c645f315f76616c7565223b4e3b733a32303a22637573746f6d5f6669656c645f325f76616c7565223b4e3b733a32303a22637573746f6d5f6669656c645f335f76616c7565223b4e3b733a32303a22637573746f6d5f6669656c645f345f76616c7565223b4e3b733a32303a22637573746f6d5f6669656c645f355f76616c7565223b4e3b733a32303a22637573746f6d5f6669656c645f365f76616c7565223b4e3b733a32303a22637573746f6d5f6669656c645f375f76616c7565223b4e3b733a32303a22637573746f6d5f6669656c645f385f76616c7565223b4e3b733a32303a22637573746f6d5f6669656c645f395f76616c7565223b4e3b733a32313a22637573746f6d5f6669656c645f31305f76616c7565223b4e3b733a31383a226f766572726964655f7461785f6e616d6573223b4e3b733a32313a226f766572726964655f7461785f70657263656e7473223b4e3b733a32343a226f766572726964655f7461785f63756d756c617469766573223b4e3b733a31383a226f766572726964655f7461785f636c617373223b4e3b733a31373a2264657461696c735f636f6c6c6170736564223b623a303b733a31323a22736b69705f776562686f6f6b223b623a303b733a363a226f6666736574223b693a303b733a353a226c696d6974223b693a32303b7d223b666f726569676e5f6c616e67756167655f746f5f6375725f6c616e67756167655f726563767c613a37353a7b733a373a22436f6e74616e74223b733a343a2243617368223b733a363a22436865717565223b733a353a22436865636b223b733a393a22576161726465626f6e223b733a393a22476966742043617264223b733a393a2242616e6b6b61617274223b733a31303a2244656269742043617264223b733a31323a224b7265646965746b61617274223b733a31313a224372656469742043617264223b733a31313a224f702072656b656e696e67223b733a31333a2253746f7265204163636f756e74223b733a31323a225469e1bb816e206de1bab774223b733a343a2243617368223b733a343a2253c3a963223b733a353a22436865636b223b733a31373a225468e1babb207175c3a02074e1bab76e67223b733a393a22476966742043617264223b733a31343a225468e1babb20676869206ee1bba3223b733a31303a2244656269742043617264223b733a31373a225468e1babb2074c3ad6e2064e1bba56e67223b733a31313a224372656469742043617264223b733a33303a2254c3a069206b686fe1baa36e2063e1bba7612063e1bbad612068c3a06e67223b733a31333a2253746f7265204163636f756e74223b733a373a2242617267656c64223b733a343a2243617368223b733a363a2253636865636b223b733a353a22436865636b223b733a393a2247757473636865696e223b733a393a22476966742043617264223b733a31313a224b756e64656e6b61727465223b733a31303a2244656269742043617264223b733a31313a224b72656469746b61727465223b733a31313a224372656469742043617264223b733a31333a224c6164656e2d4163636f756e74223b733a31333a2253746f7265204163636f756e74223b733a353a2254756e6169223b733a343a2243617368223b733a333a2243656b223b733a353a22436865636b223b733a31323a22486164696168204b61727475223b733a393a22476966742043617264223b733a31313a224b61727475204465626974223b733a31303a2244656269742043617264223b733a31323a224b6172747520437265646974223b733a31313a224372656469742043617264223b733a393a22546f6b6f20416b756e223b733a31333a2253746f7265204163636f756e74223b733a363a22e78fbee98791223b733a343a2243617368223b733a363a22e6aaa2e69fa5223b733a353a22436865636b223b733a393a22e7a6aee789a9e58da1223b733a393a22476966742043617264223b733a393a22e5809fe8a898e58da1223b733a31303a2244656269742043617264223b733a393a22e4bfa1e794a8e58da1223b733a31313a224372656469742043617264223b733a31323a22e59586e5ba97e5b8b3e8999f223b733a31333a2253746f7265204163636f756e74223b733a31303a224e756d6572c3a172696f223b733a343a2243617368223b733a31343a2256616c65206465204f6665727461223b733a393a22476966742043617264223b733a31383a2243617274c3a36f2064652044c3a96269746f223b733a31303a2244656269742043617264223b733a31393a2243617274c3a36f206465204372c3a96469746f223b733a31313a224372656469742043617264223b733a31333a22436f6e7461206461204c6f6a61223b733a31333a2253746f7265204163636f756e74223b733a383a22436f6e74616e7465223b733a343a2243617368223b733a373a2241737365676e6f223b733a353a22436865636b223b733a31323a2242756f6e6f20526567616c6f223b733a393a22476966742043617264223b733a383a2242616e636f6d6174223b733a31303a2244656269742043617264223b733a31363a224361727461206469204372656469746f223b733a31313a224372656469742043617264223b733a31343a22436f6e746f206465706f7369746f223b733a31333a2253746f7265204163636f756e74223b733a343a2243617368223b733a343a2243617368223b733a353a22436865636b223b733a353a22436865636b223b733a393a22476966742043617264223b733a393a22476966742043617264223b733a31303a2244656269742043617264223b733a31303a2244656269742043617264223b733a31313a224372656469742043617264223b733a31313a224372656469742043617264223b733a31333a2253746f7265204163636f756e74223b733a31333a2253746f7265204163636f756e74223b733a33303a22e19e9fe19eb6e19e85e19f8be19e94e19f92e19e9ae19eb6e19e80e19f8b223b733a343a2243617368223b733a393a22e19e9fe19f82e19e80223b733a353a22436865636b223b733a33333a22e19e80e19eb6e19e8fe2808be19e9ae19e84e19f92e19e9ce19eb6e19e93e19f8b223b733a393a22476966742043617264223b733a33363a22e19e94e19f90e19e8ee19f92e19e8ee2808be19ea5e19e8ee19e96e19e93e19f92e19e92223b733a31303a2244656269742043617264223b733a33333a22e19e94e19f90e19e8ee19f92e19e8ee2808be19ea5e19e8ee19e91e19eb6e19e93223b733a31313a224372656469742043617264223b733a33363a22e19e94e19f92e19e9ae19eb6e19e80e19f8be19e87e19f86e19e96e19eb6e19e80e19f8b223b733a31333a2253746f7265204163636f756e74223b733a383a22436f6d7074616e74223b733a343a2243617368223b733a373a224368c3a8717565223b733a353a22436865636b223b733a31323a22436172746520636164656175223b733a393a22476966742043617264223b733a31323a2243617274652064c3a9626974223b733a31303a2244656269742043617264223b733a31323a22436172746520637265646974223b733a31313a224372656469742043617264223b733a31343a22436f6d707465206d61676173696e223b733a31333a2253746f7265204163636f756e74223b733a383a22456665637469766f223b733a343a2243617368223b733a31373a225461726a65746120646520726567616c6f223b733a393a22476966742043617264223b733a31383a225461726a6574612064652064c3a96269746f223b733a31303a2244656269742043617264223b733a31393a225461726a657461206465206372c3a96469746f223b733a31313a224372656469742043617264223b733a31383a224cc3ad6e6561206465206372c3a96469746f223b733a31333a2253746f7265204163636f756e74223b733a363a22e78eb0e98791223b733a343a2243617368223b733a363a22e6a380e69fa5223b733a353a22436865636b223b733a393a22e7a4bce789a9e58da1223b733a393a22476966742043617264223b733a393a22e5809fe8aeb0e58da1223b733a31303a2244656269742043617264223b733a31323a22e59586e5ba97e5b890e58fb7223b733a31333a2253746f7265204163636f756e74223b733a363a22d983d8a7d8b4223b733a343a2243617368223b733a363a22d8b4d98ad983223b733a353a22436865636b223b733a31353a22d983d8b1d8aa20d987d8afd98ad8a9223b733a393a22476966742043617264223b733a32333a22d8a8d8b7d8a7d982d8a920d8a7d8a6d8aad985d8a7d986223b733a31303a2244656269742043617264223b733a32373a22d8a8d8b7d8a7d982d8a920d8a7d984d8a7d8a6d8aad985d8a7d986223b733a31313a224372656469742043617264223b733a32313a22d8add8b3d8a7d8a820d8a7d984d985d8aad8acd8b1223b733a31333a2253746f7265204163636f756e74223b7d66756c6c73637265656e7c733a313a2230223b6b6565705f616c6976657c693a313638333830383738313b),
('994684acdcb2fdad236175e88e79c310c016ebd0', '::1', 1685611459, 0x706572736f6e5f69647c733a313a2231223b6b6565705f616c6976657c693a313638353631313435393b);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_shipping_methods`
--

CREATE TABLE `tbl_pos_shipping_methods` (
  `id` int(10) NOT NULL,
  `shipping_provider_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fee` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `fee_tax_class_id` int(10) DEFAULT NULL,
  `time_in_days` int(11) DEFAULT NULL,
  `has_tracking_number` int(1) NOT NULL DEFAULT 0,
  `is_default` int(1) NOT NULL DEFAULT 0,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_shipping_providers`
--

CREATE TABLE `tbl_pos_shipping_providers` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(10) NOT NULL DEFAULT 0,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_shipping_zones`
--

CREATE TABLE `tbl_pos_shipping_zones` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fee` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `tax_class_id` int(10) DEFAULT NULL,
  `order` int(10) NOT NULL DEFAULT 0,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_store_accounts`
--

CREATE TABLE `tbl_pos_store_accounts` (
  `sno` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `transaction_amount` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `balance` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `comment` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_store_accounts_paid_sales`
--

CREATE TABLE `tbl_pos_store_accounts_paid_sales` (
  `id` int(10) NOT NULL,
  `store_account_payment_sale_id` int(10) DEFAULT NULL,
  `sale_id` int(10) DEFAULT NULL,
  `partial_payment_amount` decimal(23,10) NOT NULL DEFAULT 0.0000000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_suppliers`
--

CREATE TABLE `tbl_pos_suppliers` (
  `id` int(10) NOT NULL,
  `person_id` int(10) NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `override_default_tax` int(1) NOT NULL DEFAULT 0,
  `balance` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `tax_class_id` int(10) DEFAULT NULL,
  `custom_field_1_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_2_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_3_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_4_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_5_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_6_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_7_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_8_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_9_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `custom_field_10_value` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_suppliers_taxes`
--

CREATE TABLE `tbl_pos_suppliers_taxes` (
  `id` int(10) NOT NULL,
  `supplier_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_supplier_store_accounts`
--

CREATE TABLE `tbl_pos_supplier_store_accounts` (
  `sno` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `receiving_id` int(11) DEFAULT NULL,
  `transaction_amount` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `balance` decimal(23,10) NOT NULL DEFAULT 0.0000000000,
  `comment` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_supplier_store_accounts_paid_receivings`
--

CREATE TABLE `tbl_pos_supplier_store_accounts_paid_receivings` (
  `id` int(10) NOT NULL,
  `store_account_payment_receiving_id` int(10) DEFAULT NULL,
  `receiving_id` int(10) DEFAULT NULL,
  `partial_payment_amount` decimal(23,10) NOT NULL DEFAULT 0.0000000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_tags`
--

CREATE TABLE `tbl_pos_tags` (
  `id` int(11) NOT NULL,
  `ecommerce_tag_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_modified` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` int(1) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_tax_classes`
--

CREATE TABLE `tbl_pos_tax_classes` (
  `id` int(10) NOT NULL,
  `order` int(10) NOT NULL DEFAULT 0,
  `location_id` int(10) DEFAULT NULL,
  `deleted` int(1) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ecommerce_tax_class_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_pos_tax_classes`
--

INSERT INTO `tbl_pos_tax_classes` (`id`, `order`, `location_id`, `deleted`, `name`, `ecommerce_tax_class_id`) VALUES
(1, 1, NULL, 0, 'Taxes', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_tax_classes_taxes`
--

CREATE TABLE `tbl_pos_tax_classes_taxes` (
  `id` int(10) NOT NULL,
  `order` int(10) NOT NULL DEFAULT 0,
  `tax_class_id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` decimal(15,3) NOT NULL,
  `cumulative` int(1) NOT NULL DEFAULT 0,
  `ecommerce_tax_class_tax_rate_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pos_zips`
--

CREATE TABLE `tbl_pos_zips` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shipping_zone_id` int(10) DEFAULT NULL,
  `order` int(10) NOT NULL DEFAULT 0,
  `deleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hardik Savani', 'admin@gmail.com', NULL, '$2y$10$Ii1uW.eGnZRCrVKQZdhmuuSeWcT/fieWeNAJ0NjZrkW3ab5T1lxtq', NULL, '2023-05-26 08:14:11', '2023-05-26 08:14:11'),
(2, 'admin', 'a@gmail.com', NULL, '$2y$10$Ii1uW.eGnZRCrVKQZdhmuuSeWcT/fieWeNAJ0NjZrkW3ab5T1lxtq', NULL, '2023-05-26 09:48:48', '2023-05-26 09:48:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `tbl_pos_access`
--
ALTER TABLE `tbl_pos_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `controller` (`controller`),
  ADD KEY `phppos_access_key_fk` (`key`);

--
-- Indexes for table `tbl_pos_additional_item_numbers`
--
ALTER TABLE `tbl_pos_additional_item_numbers`
  ADD PRIMARY KEY (`item_id`,`item_number`),
  ADD UNIQUE KEY `item_number` (`item_number`),
  ADD KEY `phppos_additional_item_numbers_ibfk_2` (`item_variation_id`);

--
-- Indexes for table `tbl_pos_appointments`
--
ALTER TABLE `tbl_pos_appointments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_appointments_ibfk_1` (`appointments_type_id`),
  ADD KEY `phppos_appointments_ibfk_2` (`person_id`),
  ADD KEY `phppos_appointments_ibfk_3` (`location_id`),
  ADD KEY `phppos_appointments_ibfk_4` (`employee_id`);

--
-- Indexes for table `tbl_pos_appointment_types`
--
ALTER TABLE `tbl_pos_appointment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pos_app_files`
--
ALTER TABLE `tbl_pos_app_files`
  ADD PRIMARY KEY (`file_id`),
  ADD KEY `expires` (`expires`);

--
-- Indexes for table `tbl_pos_attributes`
--
ALTER TABLE `tbl_pos_attributes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`item_id`,`name`);

--
-- Indexes for table `tbl_pos_attribute_values`
--
ALTER TABLE `tbl_pos_attribute_values`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_attribute_id` (`name`,`attribute_id`),
  ADD KEY `phppos_attribute_values_ibfk_1` (`attribute_id`);

--
-- Indexes for table `tbl_pos_categories`
--
ALTER TABLE `tbl_pos_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `phppos_categories_ibfk_1` (`parent_id`),
  ADD KEY `phppos_categories_ibfk_2` (`image_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `tbl_pos_currency_exchange_rates`
--
ALTER TABLE `tbl_pos_currency_exchange_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pos_customers`
--
ALTER TABLE `tbl_pos_customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_number` (`account_number`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `cc_token` (`cc_token`),
  ADD KEY `phppos_customers_ibfk_2` (`tier_id`),
  ADD KEY `company_name` (`company_name`),
  ADD KEY `phppos_customers_ibfk_3` (`tax_class_id`),
  ADD KEY `custom_field_1_value` (`custom_field_1_value`),
  ADD KEY `custom_field_2_value` (`custom_field_2_value`),
  ADD KEY `custom_field_3_value` (`custom_field_3_value`),
  ADD KEY `custom_field_4_value` (`custom_field_4_value`),
  ADD KEY `custom_field_5_value` (`custom_field_5_value`),
  ADD KEY `custom_field_6_value` (`custom_field_6_value`),
  ADD KEY `custom_field_7_value` (`custom_field_7_value`),
  ADD KEY `custom_field_8_value` (`custom_field_8_value`),
  ADD KEY `custom_field_9_value` (`custom_field_9_value`),
  ADD KEY `custom_field_10_value` (`custom_field_10_value`),
  ADD KEY `phppos_customers_ibfk_4` (`location_id`);

--
-- Indexes for table `tbl_pos_customers_series`
--
ALTER TABLE `tbl_pos_customers_series`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_customers_series_ibfk_1` (`item_id`),
  ADD KEY `phppos_customers_series_ibfk_2` (`customer_id`),
  ADD KEY `phppos_customers_series_ibfk_3` (`sale_id`);

--
-- Indexes for table `tbl_pos_customers_series_log`
--
ALTER TABLE `tbl_pos_customers_series_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_customers_series_log_ibfk_1` (`series_id`);

--
-- Indexes for table `tbl_pos_customers_taxes`
--
ALTER TABLE `tbl_pos_customers_taxes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_tax` (`customer_id`,`name`,`percent`);

--
-- Indexes for table `tbl_pos_damaged_items_log`
--
ALTER TABLE `tbl_pos_damaged_items_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_damaged_items_log_ibfk_1` (`item_id`),
  ADD KEY `phppos_damaged_items_log_ibfk_2` (`item_variation_id`),
  ADD KEY `phppos_damaged_items_log_ibfk_3` (`sale_id`),
  ADD KEY `phppos_damaged_items_log_ibfk_4` (`location_id`);

--
-- Indexes for table `tbl_pos_ecommerce_locations`
--
ALTER TABLE `tbl_pos_ecommerce_locations`
  ADD PRIMARY KEY (`location_id`);

--
-- Indexes for table `tbl_pos_employees`
--
ALTER TABLE `tbl_pos_employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `employee_number` (`employee_number`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `custom_field_1_value` (`custom_field_1_value`),
  ADD KEY `custom_field_2_value` (`custom_field_2_value`),
  ADD KEY `custom_field_3_value` (`custom_field_3_value`),
  ADD KEY `custom_field_4_value` (`custom_field_4_value`),
  ADD KEY `custom_field_5_value` (`custom_field_5_value`),
  ADD KEY `custom_field_6_value` (`custom_field_6_value`),
  ADD KEY `custom_field_7_value` (`custom_field_7_value`),
  ADD KEY `custom_field_8_value` (`custom_field_8_value`),
  ADD KEY `custom_field_9_value` (`custom_field_9_value`),
  ADD KEY `custom_field_10_value` (`custom_field_10_value`);

--
-- Indexes for table `tbl_pos_employees_app_config`
--
ALTER TABLE `tbl_pos_employees_app_config`
  ADD PRIMARY KEY (`employee_id`,`key`);

--
-- Indexes for table `tbl_pos_employees_locations`
--
ALTER TABLE `tbl_pos_employees_locations`
  ADD PRIMARY KEY (`employee_id`,`location_id`),
  ADD KEY `phppos_employees_locations_ibfk_2` (`location_id`);

--
-- Indexes for table `tbl_pos_employees_reset_password`
--
ALTER TABLE `tbl_pos_employees_reset_password`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_employees_reset_password_ibfk_1` (`employee_id`);

--
-- Indexes for table `tbl_pos_employees_time_clock`
--
ALTER TABLE `tbl_pos_employees_time_clock`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_employees_time_clock_ibfk_1` (`employee_id`),
  ADD KEY `phppos_employees_time_clock_ibfk_2` (`location_id`);

--
-- Indexes for table `tbl_pos_employees_time_off`
--
ALTER TABLE `tbl_pos_employees_time_off`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_employees_time_off_ibfk_1` (`employee_requested_person_id`),
  ADD KEY `phppos_employees_time_off_ibfk_2` (`employee_approved_person_id`),
  ADD KEY `phppos_employees_time_off_ibfk_3` (`employee_requested_location_id`);

--
-- Indexes for table `tbl_pos_employee_registers`
--
ALTER TABLE `tbl_pos_employee_registers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_employee_registers_ibfk_1` (`employee_id`),
  ADD KEY `phppos_employee_registers_ibfk_2` (`register_id`);

--
-- Indexes for table `tbl_pos_expenses`
--
ALTER TABLE `tbl_pos_expenses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `approved_employee_id` (`approved_employee_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `expense_type` (`expense_type`),
  ADD KEY `expense_date` (`expense_date`),
  ADD KEY `expense_amount` (`expense_amount`),
  ADD KEY `expense_description` (`expense_description`(255)),
  ADD KEY `expense_reason` (`expense_reason`),
  ADD KEY `expense_note` (`expense_note`(255));

--
-- Indexes for table `tbl_pos_giftcards`
--
ALTER TABLE `tbl_pos_giftcards`
  ADD PRIMARY KEY (`giftcard_id`),
  ADD UNIQUE KEY `giftcard_number` (`giftcard_number`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `phppos_giftcards_ibfk_1` (`customer_id`),
  ADD KEY `description` (`description`(255));

--
-- Indexes for table `tbl_pos_giftcards_log`
--
ALTER TABLE `tbl_pos_giftcards_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_giftcards_log_ibfk_1` (`giftcard_id`);

--
-- Indexes for table `tbl_pos_grid_hidden_categories`
--
ALTER TABLE `tbl_pos_grid_hidden_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_grid` (`category_id`,`location_id`),
  ADD KEY `phppos_grid_hidden_categories_ibfk_2` (`location_id`);

--
-- Indexes for table `tbl_pos_grid_hidden_items`
--
ALTER TABLE `tbl_pos_grid_hidden_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_grid` (`item_id`,`location_id`),
  ADD KEY `phppos_grid_hidden_items_ibfk_2` (`location_id`);

--
-- Indexes for table `tbl_pos_grid_hidden_item_kits`
--
ALTER TABLE `tbl_pos_grid_hidden_item_kits`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_grid` (`item_kit_id`,`location_id`),
  ADD KEY `phppos_grid_hidden_item_kits_ibfk_2` (`location_id`);

--
-- Indexes for table `tbl_pos_grid_hidden_tags`
--
ALTER TABLE `tbl_pos_grid_hidden_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_grid` (`tag_id`,`location_id`),
  ADD KEY `phppos_grid_hidden_tags_ibfk_2` (`location_id`);

--
-- Indexes for table `tbl_pos_inventory`
--
ALTER TABLE `tbl_pos_inventory`
  ADD PRIMARY KEY (`trans_id`),
  ADD KEY `phppos_inventory_ibfk_1` (`trans_items`),
  ADD KEY `phppos_inventory_ibfk_2` (`trans_user`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `trans_date` (`trans_date`,`trans_inventory`,`location_id`),
  ADD KEY `phppos_inventory_ibfk_4` (`item_variation_id`),
  ADD KEY `phppos_inventory_custom` (`trans_items`,`location_id`,`trans_date`,`item_variation_id`,`trans_id`);

--
-- Indexes for table `tbl_pos_inventory_counts`
--
ALTER TABLE `tbl_pos_inventory_counts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_inventory_counts_ibfk_1` (`employee_id`),
  ADD KEY `phppos_inventory_counts_ibfk_2` (`location_id`);

--
-- Indexes for table `tbl_pos_inventory_counts_items`
--
ALTER TABLE `tbl_pos_inventory_counts_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_inventory_counts_items_ibfk_1` (`inventory_counts_id`),
  ADD KEY `phppos_inventory_counts_items_ibfk_2` (`item_id`),
  ADD KEY `inventory_counts_items_ibfk_3` (`item_variation_id`);

--
-- Indexes for table `tbl_pos_items`
--
ALTER TABLE `tbl_pos_items`
  ADD PRIMARY KEY (`item_id`),
  ADD UNIQUE KEY `item_number` (`item_number`),
  ADD UNIQUE KEY `product_id` (`product_id`),
  ADD KEY `phppos_items_ibfk_1` (`supplier_id`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `phppos_items_ibfk_3` (`category_id`),
  ADD KEY `phppos_items_ibfk_4` (`manufacturer_id`),
  ADD KEY `phppos_items_ibfk_5` (`ecommerce_product_id`),
  ADD KEY `description` (`description`(255)),
  ADD KEY `size` (`size`),
  ADD KEY `reorder_level` (`reorder_level`),
  ADD KEY `cost_price` (`cost_price`),
  ADD KEY `unit_price` (`unit_price`),
  ADD KEY `promo_price` (`promo_price`),
  ADD KEY `last_modified` (`last_modified`),
  ADD KEY `name` (`name`),
  ADD KEY `phppos_items_ibfk_6` (`tax_class_id`),
  ADD KEY `deleted_system_item` (`deleted`,`system_item`),
  ADD KEY `custom_field_1_value` (`custom_field_1_value`),
  ADD KEY `custom_field_2_value` (`custom_field_2_value`),
  ADD KEY `custom_field_3_value` (`custom_field_3_value`),
  ADD KEY `custom_field_4_value` (`custom_field_4_value`),
  ADD KEY `custom_field_5_value` (`custom_field_5_value`),
  ADD KEY `custom_field_6_value` (`custom_field_6_value`),
  ADD KEY `custom_field_7_value` (`custom_field_7_value`),
  ADD KEY `custom_field_8_value` (`custom_field_8_value`),
  ADD KEY `custom_field_9_value` (`custom_field_9_value`),
  ADD KEY `custom_field_10_value` (`custom_field_10_value`),
  ADD KEY `verify_age` (`verify_age`),
  ADD KEY `phppos_items_ibfk_7` (`main_image_id`),
  ADD KEY `item_inactive_index` (`item_inactive`),
  ADD KEY `tags` (`tags`),
  ADD KEY `is_favorite_index` (`is_favorite`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `full_search` (`name`,`item_number`,`product_id`,`description`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `name_search` (`name`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `item_number_search` (`item_number`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `product_id_search` (`product_id`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `description_search` (`description`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `size_search` (`size`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `custom_field_1_value_search` (`custom_field_1_value`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `custom_field_2_value_search` (`custom_field_2_value`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `custom_field_3_value_search` (`custom_field_3_value`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `custom_field_4_value_search` (`custom_field_4_value`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `custom_field_5_value_search` (`custom_field_5_value`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `custom_field_6_value_search` (`custom_field_6_value`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `custom_field_7_value_search` (`custom_field_7_value`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `custom_field_8_value_search` (`custom_field_8_value`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `custom_field_9_value_search` (`custom_field_9_value`);
ALTER TABLE `tbl_pos_items` ADD FULLTEXT KEY `custom_field_10_value_search` (`custom_field_10_value`);

--
-- Indexes for table `tbl_pos_items_modifiers`
--
ALTER TABLE `tbl_pos_items_modifiers`
  ADD PRIMARY KEY (`item_id`,`modifier_id`),
  ADD KEY `phppos_items_modifiers_ibfk_1` (`modifier_id`);

--
-- Indexes for table `tbl_pos_items_pricing_history`
--
ALTER TABLE `tbl_pos_items_pricing_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_items_pricing_history_ibfk_1` (`item_id`),
  ADD KEY `phppos_items_pricing_history_ibfk_2` (`item_variation_id`),
  ADD KEY `phppos_items_pricing_history_ibfk_3` (`location_id`),
  ADD KEY `phppos_items_pricing_history_ibfk_4` (`employee_id`);

--
-- Indexes for table `tbl_pos_items_quantity_units`
--
ALTER TABLE `tbl_pos_items_quantity_units`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `quantity_unit_item_number` (`quantity_unit_item_number`),
  ADD KEY `phppos_items_quantity_units_ibfk_1` (`item_id`);

--
-- Indexes for table `tbl_pos_items_serial_numbers`
--
ALTER TABLE `tbl_pos_items_serial_numbers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `serial_number` (`serial_number`),
  ADD KEY `phppos_items_serial_numbers_ibfk_1` (`item_id`);

--
-- Indexes for table `tbl_pos_items_tags`
--
ALTER TABLE `tbl_pos_items_tags`
  ADD PRIMARY KEY (`item_id`,`tag_id`),
  ADD KEY `phppos_items_tags_ibfk_2` (`tag_id`);

--
-- Indexes for table `tbl_pos_items_taxes`
--
ALTER TABLE `tbl_pos_items_taxes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_tax` (`item_id`,`name`,`percent`);

--
-- Indexes for table `tbl_pos_items_tier_prices`
--
ALTER TABLE `tbl_pos_items_tier_prices`
  ADD PRIMARY KEY (`tier_id`,`item_id`),
  ADD KEY `phppos_items_tier_prices_ibfk_2` (`item_id`);

--
-- Indexes for table `tbl_pos_item_attributes`
--
ALTER TABLE `tbl_pos_item_attributes`
  ADD PRIMARY KEY (`attribute_id`,`item_id`),
  ADD KEY `phppos_item_attributes_ibfk_1` (`item_id`);

--
-- Indexes for table `tbl_pos_item_attribute_values`
--
ALTER TABLE `tbl_pos_item_attribute_values`
  ADD PRIMARY KEY (`attribute_value_id`,`item_id`),
  ADD KEY `phppos_item_attribute_values_ibfk_1` (`item_id`);

--
-- Indexes for table `tbl_pos_item_images`
--
ALTER TABLE `tbl_pos_item_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_item_images_ibfk_1` (`item_id`),
  ADD KEY `phppos_item_images_ibfk_2` (`image_id`),
  ADD KEY `phppos_item_images_ibfk_3` (`item_variation_id`);

--
-- Indexes for table `tbl_pos_item_kits`
--
ALTER TABLE `tbl_pos_item_kits`
  ADD PRIMARY KEY (`item_kit_id`),
  ADD UNIQUE KEY `item_kit_number` (`item_kit_number`),
  ADD UNIQUE KEY `product_id` (`product_id`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `phppos_item_kits_ibfk_1` (`category_id`),
  ADD KEY `phppos_item_kits_ibfk_2` (`manufacturer_id`),
  ADD KEY `name` (`name`),
  ADD KEY `description` (`description`),
  ADD KEY `cost_price` (`cost_price`),
  ADD KEY `unit_price` (`unit_price`),
  ADD KEY `phppos_item_kits_ibfk_3` (`tax_class_id`),
  ADD KEY `custom_field_1_value` (`custom_field_1_value`),
  ADD KEY `custom_field_2_value` (`custom_field_2_value`),
  ADD KEY `custom_field_3_value` (`custom_field_3_value`),
  ADD KEY `custom_field_4_value` (`custom_field_4_value`),
  ADD KEY `custom_field_5_value` (`custom_field_5_value`),
  ADD KEY `custom_field_6_value` (`custom_field_6_value`),
  ADD KEY `custom_field_7_value` (`custom_field_7_value`),
  ADD KEY `custom_field_8_value` (`custom_field_8_value`),
  ADD KEY `custom_field_9_value` (`custom_field_9_value`),
  ADD KEY `custom_field_10_value` (`custom_field_10_value`),
  ADD KEY `verify_age` (`verify_age`),
  ADD KEY `phppos_item_kits_ibfk_4` (`main_image_id`),
  ADD KEY `item_kit_inactive_index` (`item_kit_inactive`),
  ADD KEY `is_favorite_index` (`is_favorite`);

--
-- Indexes for table `tbl_pos_item_kits_modifiers`
--
ALTER TABLE `tbl_pos_item_kits_modifiers`
  ADD PRIMARY KEY (`item_kit_id`,`modifier_id`),
  ADD KEY `phppos_item_kits_modifiers_ibfk_1` (`modifier_id`);

--
-- Indexes for table `tbl_pos_item_kits_pricing_history`
--
ALTER TABLE `tbl_pos_item_kits_pricing_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_item_kits_pricing_history_ibfk_1` (`item_kit_id`),
  ADD KEY `phppos_item_kits_pricing_history_ibfk_2` (`location_id`),
  ADD KEY `phppos_item_kits_pricing_history_ibfk_3` (`employee_id`);

--
-- Indexes for table `tbl_pos_item_kits_tags`
--
ALTER TABLE `tbl_pos_item_kits_tags`
  ADD PRIMARY KEY (`item_kit_id`,`tag_id`),
  ADD KEY `phppos_item_kits_tags_ibfk_2` (`tag_id`);

--
-- Indexes for table `tbl_pos_item_kits_taxes`
--
ALTER TABLE `tbl_pos_item_kits_taxes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_tax` (`item_kit_id`,`name`,`percent`);

--
-- Indexes for table `tbl_pos_item_kits_tier_prices`
--
ALTER TABLE `tbl_pos_item_kits_tier_prices`
  ADD PRIMARY KEY (`tier_id`,`item_kit_id`),
  ADD KEY `phppos_item_kits_tier_prices_ibfk_2` (`item_kit_id`);

--
-- Indexes for table `tbl_pos_item_kit_images`
--
ALTER TABLE `tbl_pos_item_kit_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_item_kit_images_ibfk_1` (`item_kit_id`),
  ADD KEY `phppos_item_kit_images_ibfk_2` (`image_id`);

--
-- Indexes for table `tbl_pos_item_kit_items`
--
ALTER TABLE `tbl_pos_item_kit_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_item_kit_items_ibfk_1` (`item_kit_id`),
  ADD KEY `phppos_item_kit_items_ibfk_2` (`item_id`),
  ADD KEY `phppos_item_kit_items_ibfk_3` (`item_variation_id`);

--
-- Indexes for table `tbl_pos_item_kit_item_kits`
--
ALTER TABLE `tbl_pos_item_kit_item_kits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_item_kit_item_kits_ibfk_1` (`item_kit_id`),
  ADD KEY `phppos_item_kit_item_kits_ibfk_2` (`item_kit_item_kit`);

--
-- Indexes for table `tbl_pos_item_variations`
--
ALTER TABLE `tbl_pos_item_variations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_number` (`item_number`),
  ADD KEY `phppos_item_variations_ibfk_1` (`item_id`),
  ADD KEY `phppos_item_variations_ibfk_2` (`ecommerce_variation_id`);
ALTER TABLE `tbl_pos_item_variations` ADD FULLTEXT KEY `name_search` (`name`);

--
-- Indexes for table `tbl_pos_item_variation_attribute_values`
--
ALTER TABLE `tbl_pos_item_variation_attribute_values`
  ADD PRIMARY KEY (`attribute_value_id`,`item_variation_id`),
  ADD KEY `phppos_item_variation_attribute_values_ibfk_2` (`item_variation_id`);

--
-- Indexes for table `tbl_pos_keys`
--
ALTER TABLE `tbl_pos_keys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`),
  ADD KEY `phppos_keys_user_id_fk` (`user_id`);

--
-- Indexes for table `tbl_pos_limits`
--
ALTER TABLE `tbl_pos_limits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uri` (`uri`),
  ADD KEY `phppos_limits_api_key_fk` (`api_key`);

--
-- Indexes for table `tbl_pos_locations`
--
ALTER TABLE `tbl_pos_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `phppos_locations_ibfk_1` (`company_logo`),
  ADD KEY `name` (`name`(255)),
  ADD KEY `address` (`address`(255)),
  ADD KEY `phone` (`phone`(255)),
  ADD KEY `email` (`email`(255)),
  ADD KEY `phppos_locations_ibfk_2` (`tax_class_id`);

--
-- Indexes for table `tbl_pos_location_items`
--
ALTER TABLE `tbl_pos_location_items`
  ADD PRIMARY KEY (`location_id`,`item_id`),
  ADD KEY `phppos_location_items_ibfk_2` (`item_id`),
  ADD KEY `quantity` (`quantity`),
  ADD KEY `phppos_location_items_ibfk_3` (`tax_class_id`);

--
-- Indexes for table `tbl_pos_location_items_taxes`
--
ALTER TABLE `tbl_pos_location_items_taxes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_tax` (`location_id`,`item_id`,`name`,`percent`),
  ADD KEY `phppos_location_items_taxes_ibfk_2` (`item_id`);

--
-- Indexes for table `tbl_pos_location_items_tier_prices`
--
ALTER TABLE `tbl_pos_location_items_tier_prices`
  ADD PRIMARY KEY (`tier_id`,`item_id`,`location_id`),
  ADD KEY `phppos_location_items_tier_prices_ibfk_2` (`location_id`),
  ADD KEY `phppos_location_items_tier_prices_ibfk_3` (`item_id`);

--
-- Indexes for table `tbl_pos_location_item_kits`
--
ALTER TABLE `tbl_pos_location_item_kits`
  ADD PRIMARY KEY (`location_id`,`item_kit_id`),
  ADD KEY `phppos_location_item_kits_ibfk_2` (`item_kit_id`),
  ADD KEY `phppos_location_item_kits_ibfk_3` (`tax_class_id`);

--
-- Indexes for table `tbl_pos_location_item_kits_taxes`
--
ALTER TABLE `tbl_pos_location_item_kits_taxes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_tax` (`location_id`,`item_kit_id`,`name`,`percent`),
  ADD KEY `phppos_location_item_kits_taxes_ibfk_2` (`item_kit_id`);

--
-- Indexes for table `tbl_pos_location_item_kits_tier_prices`
--
ALTER TABLE `tbl_pos_location_item_kits_tier_prices`
  ADD PRIMARY KEY (`tier_id`,`item_kit_id`,`location_id`),
  ADD KEY `phppos_location_item_kits_tier_prices_ibfk_2` (`location_id`),
  ADD KEY `phppos_location_item_kits_tier_prices_ibfk_3` (`item_kit_id`);

--
-- Indexes for table `tbl_pos_location_item_variations`
--
ALTER TABLE `tbl_pos_location_item_variations`
  ADD PRIMARY KEY (`item_variation_id`,`location_id`),
  ADD KEY `phppos_item_attribute_location_values_ibfk_2` (`location_id`);

--
-- Indexes for table `tbl_pos_logs`
--
ALTER TABLE `tbl_pos_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pos_manufacturers`
--
ALTER TABLE `tbl_pos_manufacturers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `tbl_pos_messages`
--
ALTER TABLE `tbl_pos_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_messages_ibfk_1` (`sender_id`),
  ADD KEY `phppos_messages_key_1` (`deleted`,`created_at`,`id`);

--
-- Indexes for table `tbl_pos_message_receiver`
--
ALTER TABLE `tbl_pos_message_receiver`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_message_receiver_ibfk_2` (`receiver_id`),
  ADD KEY `phppos_message_receiver_key_1` (`message_id`,`receiver_id`);

--
-- Indexes for table `tbl_pos_modifiers`
--
ALTER TABLE `tbl_pos_modifiers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `sort_index` (`deleted`,`sort_order`);

--
-- Indexes for table `tbl_pos_modifier_items`
--
ALTER TABLE `tbl_pos_modifier_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_modifier_items_ibfk_1` (`modifier_id`),
  ADD KEY `sort_index` (`deleted`,`modifier_id`,`sort_order`);

--
-- Indexes for table `tbl_pos_modules`
--
ALTER TABLE `tbl_pos_modules`
  ADD PRIMARY KEY (`module_id`),
  ADD UNIQUE KEY `desc_lang_key` (`desc_lang_key`),
  ADD UNIQUE KEY `name_lang_key` (`name_lang_key`);

--
-- Indexes for table `tbl_pos_modules_actions`
--
ALTER TABLE `tbl_pos_modules_actions`
  ADD PRIMARY KEY (`action_id`,`module_id`),
  ADD KEY `phppos_modules_actions_ibfk_1` (`module_id`);

--
-- Indexes for table `tbl_pos_people`
--
ALTER TABLE `tbl_pos_people`
  ADD PRIMARY KEY (`person_id`),
  ADD KEY `phppos_people_ibfk_1` (`image_id`),
  ADD KEY `first_name` (`first_name`),
  ADD KEY `last_name` (`last_name`),
  ADD KEY `email` (`email`),
  ADD KEY `phone_number` (`phone_number`),
  ADD KEY `full_name` (`full_name`(255));

--
-- Indexes for table `tbl_pos_people_files`
--
ALTER TABLE `tbl_pos_people_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_people_files_ibfk_1` (`file_id`);

--
-- Indexes for table `tbl_pos_permissions_actions_locations`
--
ALTER TABLE `tbl_pos_permissions_actions_locations`
  ADD PRIMARY KEY (`module_id`,`person_id`,`action_id`,`location_id`),
  ADD KEY `phppos_permissions_actions_locations_ibfk_2` (`person_id`),
  ADD KEY `phppos_permissions_actions_locations_ibfk_3` (`action_id`),
  ADD KEY `phppos_permissions_actions_locations_ibfk_4` (`location_id`);

--
-- Indexes for table `tbl_pos_permissions_locations`
--
ALTER TABLE `tbl_pos_permissions_locations`
  ADD PRIMARY KEY (`module_id`,`person_id`,`location_id`),
  ADD KEY `phppos_permissions_locations_ibfk_1` (`person_id`),
  ADD KEY `phppos_permissions_locations_ibfk_3` (`location_id`);

--
-- Indexes for table `tbl_pos_price_rules`
--
ALTER TABLE `tbl_pos_price_rules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `start_date` (`start_date`),
  ADD KEY `end_date` (`end_date`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `tbl_pos_price_rules_categories`
--
ALTER TABLE `tbl_pos_price_rules_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_price_rules_categories_ibfk_1` (`rule_id`),
  ADD KEY `phppos_price_rules_categories_ibfk_2` (`category_id`);

--
-- Indexes for table `tbl_pos_price_rules_items`
--
ALTER TABLE `tbl_pos_price_rules_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_price_rules_items_ibfk_1` (`rule_id`),
  ADD KEY `phppos_price_rules_items_ibfk_2` (`item_id`);

--
-- Indexes for table `tbl_pos_price_rules_item_kits`
--
ALTER TABLE `tbl_pos_price_rules_item_kits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_price_rules_item_kits_ibfk_1` (`rule_id`),
  ADD KEY `phppos_price_rules_item_kits_ibfk_2` (`item_kit_id`);

--
-- Indexes for table `tbl_pos_price_rules_locations`
--
ALTER TABLE `tbl_pos_price_rules_locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_price_rules_locations_ibfk_1` (`rule_id`),
  ADD KEY `phppos_price_rules_locations_ibfk_2` (`location_id`);

--
-- Indexes for table `tbl_pos_price_rules_manufacturers`
--
ALTER TABLE `tbl_pos_price_rules_manufacturers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_price_rules_manufacturers_ibfk_1` (`rule_id`),
  ADD KEY `phppos_price_rules_manufacturers_ibfk_2` (`manufacturer_id`);

--
-- Indexes for table `tbl_pos_price_rules_price_breaks`
--
ALTER TABLE `tbl_pos_price_rules_price_breaks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_price_rules_custom_ibfk_1` (`rule_id`);

--
-- Indexes for table `tbl_pos_price_rules_tags`
--
ALTER TABLE `tbl_pos_price_rules_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_price_rules_tags_ibfk_1` (`rule_id`),
  ADD KEY `phppos_price_rules_tags_ibfk_2` (`tag_id`);

--
-- Indexes for table `tbl_pos_price_tiers`
--
ALTER TABLE `tbl_pos_price_tiers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pos_receivings`
--
ALTER TABLE `tbl_pos_receivings`
  ADD PRIMARY KEY (`receiving_id`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `transfer_to_location_id` (`transfer_to_location_id`),
  ADD KEY `recv_search` (`location_id`,`deleted`,`receiving_time`,`suspended`,`store_account_payment`,`total_quantity_purchased`),
  ADD KEY `custom_field_1_value` (`custom_field_1_value`),
  ADD KEY `custom_field_2_value` (`custom_field_2_value`),
  ADD KEY `custom_field_3_value` (`custom_field_3_value`),
  ADD KEY `custom_field_4_value` (`custom_field_4_value`),
  ADD KEY `custom_field_5_value` (`custom_field_5_value`),
  ADD KEY `custom_field_6_value` (`custom_field_6_value`),
  ADD KEY `custom_field_7_value` (`custom_field_7_value`),
  ADD KEY `custom_field_8_value` (`custom_field_8_value`),
  ADD KEY `custom_field_9_value` (`custom_field_9_value`),
  ADD KEY `custom_field_10_value` (`custom_field_10_value`);

--
-- Indexes for table `tbl_pos_receivings_items`
--
ALTER TABLE `tbl_pos_receivings_items`
  ADD PRIMARY KEY (`receiving_id`,`item_id`,`line`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `phppos_receivings_items_ibfk_3` (`item_variation_id`),
  ADD KEY `phppos_receivings_items_ibfk_4` (`items_quantity_units_id`);

--
-- Indexes for table `tbl_pos_receivings_items_taxes`
--
ALTER TABLE `tbl_pos_receivings_items_taxes`
  ADD PRIMARY KEY (`receiving_id`,`item_id`,`line`,`name`,`percent`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `tbl_pos_receivings_payments`
--
ALTER TABLE `tbl_pos_receivings_payments`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `receiving_id` (`receiving_id`),
  ADD KEY `payment_date` (`payment_date`);

--
-- Indexes for table `tbl_pos_registers`
--
ALTER TABLE `tbl_pos_registers`
  ADD PRIMARY KEY (`register_id`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `phppos_registers_ibfk_1` (`location_id`);

--
-- Indexes for table `tbl_pos_registers_cart`
--
ALTER TABLE `tbl_pos_registers_cart`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `register_id` (`register_id`);

--
-- Indexes for table `tbl_pos_register_currency_denominations`
--
ALTER TABLE `tbl_pos_register_currency_denominations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pos_register_log`
--
ALTER TABLE `tbl_pos_register_log`
  ADD PRIMARY KEY (`register_log_id`),
  ADD KEY `phppos_register_log_ibfk_1` (`employee_id_open`),
  ADD KEY `phppos_register_log_ibfk_2` (`register_id`),
  ADD KEY `phppos_register_log_ibfk_3` (`employee_id_close`);

--
-- Indexes for table `tbl_pos_register_log_audit`
--
ALTER TABLE `tbl_pos_register_log_audit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `register_log_audit_ibfk_1` (`register_log_id`),
  ADD KEY `register_log_audit_ibfk_2` (`employee_id`);

--
-- Indexes for table `tbl_pos_register_log_denoms`
--
ALTER TABLE `tbl_pos_register_log_denoms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_register_log_denoms_ibfk_1` (`register_log_id`),
  ADD KEY `phppos_register_log_denoms_ibfk_2` (`register_currency_denominations_id`);

--
-- Indexes for table `tbl_pos_register_log_payments`
--
ALTER TABLE `tbl_pos_register_log_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_register_log_payments_ibfk_1` (`register_log_id`);

--
-- Indexes for table `tbl_pos_sales`
--
ALTER TABLE `tbl_pos_sales`
  ADD PRIMARY KEY (`sale_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `phppos_sales_ibfk_4` (`deleted_by`),
  ADD KEY `phppos_sales_ibfk_5` (`tier_id`),
  ADD KEY `phppos_sales_ibfk_7` (`register_id`),
  ADD KEY `phppos_sales_ibfk_6` (`sold_by_employee_id`),
  ADD KEY `phppos_sales_ibfk_8` (`signature_image_id`),
  ADD KEY `was_layaway` (`was_layaway`),
  ADD KEY `was_estimate` (`was_estimate`),
  ADD KEY `phppos_sales_ibfk_9` (`rule_id`),
  ADD KEY `sales_search` (`location_id`,`deleted`,`sale_time`,`suspended`,`store_account_payment`,`total_quantity_purchased`),
  ADD KEY `phppos_sales_ibfk_10` (`suspended`),
  ADD KEY `custom_field_1_value` (`custom_field_1_value`),
  ADD KEY `custom_field_2_value` (`custom_field_2_value`),
  ADD KEY `custom_field_3_value` (`custom_field_3_value`),
  ADD KEY `custom_field_4_value` (`custom_field_4_value`),
  ADD KEY `custom_field_5_value` (`custom_field_5_value`),
  ADD KEY `custom_field_6_value` (`custom_field_6_value`),
  ADD KEY `custom_field_7_value` (`custom_field_7_value`),
  ADD KEY `custom_field_8_value` (`custom_field_8_value`),
  ADD KEY `custom_field_9_value` (`custom_field_9_value`),
  ADD KEY `custom_field_10_value` (`custom_field_10_value`),
  ADD KEY `phppos_sales_ibfk_11` (`return_sale_id`);

--
-- Indexes for table `tbl_pos_sales_coupons`
--
ALTER TABLE `tbl_pos_sales_coupons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_sales_coupons_ibfk_1` (`sale_id`),
  ADD KEY `phppos_sales_coupons_ibfk_2` (`rule_id`);

--
-- Indexes for table `tbl_pos_sales_deliveries`
--
ALTER TABLE `tbl_pos_sales_deliveries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `search_index` (`status`,`shipping_address_person_id`),
  ADD KEY `phppos_sales_deliveries_ibfk_1` (`sale_id`),
  ADD KEY `phppos_sales_deliveries_ibfk_2` (`shipping_address_person_id`),
  ADD KEY `phppos_sales_deliveries_ibfk_3` (`shipping_method_id`),
  ADD KEY `phppos_sales_deliveries_ibfk_4` (`shipping_zone_id`),
  ADD KEY `phppos_sales_deliveries_ibfk_5` (`tax_class_id`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `phppos_sales_deliveries_ibfk_6` (`delivery_employee_person_id`);

--
-- Indexes for table `tbl_pos_sales_items`
--
ALTER TABLE `tbl_pos_sales_items`
  ADD PRIMARY KEY (`sale_id`,`item_id`,`line`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `phppos_sales_items_ibfk_3` (`rule_id`),
  ADD KEY `phppos_sales_items_ibfk_4` (`item_variation_id`),
  ADD KEY `phppos_sales_items_ibfk_5` (`series_id`),
  ADD KEY `phppos_sales_items_ibfk_6` (`items_quantity_units_id`);

--
-- Indexes for table `tbl_pos_sales_items_modifier_items`
--
ALTER TABLE `tbl_pos_sales_items_modifier_items`
  ADD PRIMARY KEY (`item_id`,`sale_id`,`line`,`modifier_item_id`),
  ADD KEY `phppos_sales_items_modifier_items_ibfk_2` (`sale_id`),
  ADD KEY `phppos_sales_items_modifier_items_ibfk_3` (`modifier_item_id`);

--
-- Indexes for table `tbl_pos_sales_items_taxes`
--
ALTER TABLE `tbl_pos_sales_items_taxes`
  ADD PRIMARY KEY (`sale_id`,`item_id`,`line`,`name`,`percent`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `tbl_pos_sales_item_kits`
--
ALTER TABLE `tbl_pos_sales_item_kits`
  ADD PRIMARY KEY (`sale_id`,`item_kit_id`,`line`),
  ADD KEY `item_kit_id` (`item_kit_id`),
  ADD KEY `phppos_sales_item_kits_ibfk_3` (`rule_id`);

--
-- Indexes for table `tbl_pos_sales_item_kits_modifier_items`
--
ALTER TABLE `tbl_pos_sales_item_kits_modifier_items`
  ADD PRIMARY KEY (`item_kit_id`,`sale_id`,`line`,`modifier_item_id`),
  ADD KEY `phppos_sales_item_kits_modifier_items_ibfk_2` (`sale_id`),
  ADD KEY `phppos_sales_item_kits_modifier_items_ibfk_3` (`modifier_item_id`);

--
-- Indexes for table `tbl_pos_sales_item_kits_taxes`
--
ALTER TABLE `tbl_pos_sales_item_kits_taxes`
  ADD PRIMARY KEY (`sale_id`,`item_kit_id`,`line`,`name`,`percent`),
  ADD KEY `item_id` (`item_kit_id`);

--
-- Indexes for table `tbl_pos_sales_payments`
--
ALTER TABLE `tbl_pos_sales_payments`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `payment_date` (`payment_date`);

--
-- Indexes for table `tbl_pos_sale_types`
--
ALTER TABLE `tbl_pos_sale_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pos_sessions`
--
ALTER TABLE `tbl_pos_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `tbl_pos_shipping_methods`
--
ALTER TABLE `tbl_pos_shipping_methods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_shipping_methods_ibfk_1` (`shipping_provider_id`),
  ADD KEY `phppos_shipping_methods_ibfk_2` (`fee_tax_class_id`);

--
-- Indexes for table `tbl_pos_shipping_providers`
--
ALTER TABLE `tbl_pos_shipping_providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_pos_shipping_zones`
--
ALTER TABLE `tbl_pos_shipping_zones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_shipping_zones_ibfk_1` (`tax_class_id`);

--
-- Indexes for table `tbl_pos_store_accounts`
--
ALTER TABLE `tbl_pos_store_accounts`
  ADD PRIMARY KEY (`sno`),
  ADD KEY `phppos_store_accounts_ibfk_1` (`sale_id`),
  ADD KEY `phppos_store_accounts_ibfk_2` (`customer_id`);

--
-- Indexes for table `tbl_pos_store_accounts_paid_sales`
--
ALTER TABLE `tbl_pos_store_accounts_paid_sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_store_accounts_sales_ibfk_1` (`sale_id`),
  ADD KEY `phppos_store_accounts_sales_ibfk_2` (`store_account_payment_sale_id`);

--
-- Indexes for table `tbl_pos_suppliers`
--
ALTER TABLE `tbl_pos_suppliers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_number` (`account_number`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `deleted` (`deleted`),
  ADD KEY `phppos_suppliers_ibfk_2` (`tax_class_id`),
  ADD KEY `custom_field_1_value` (`custom_field_1_value`),
  ADD KEY `custom_field_2_value` (`custom_field_2_value`),
  ADD KEY `custom_field_3_value` (`custom_field_3_value`),
  ADD KEY `custom_field_4_value` (`custom_field_4_value`),
  ADD KEY `custom_field_5_value` (`custom_field_5_value`),
  ADD KEY `custom_field_6_value` (`custom_field_6_value`),
  ADD KEY `custom_field_7_value` (`custom_field_7_value`),
  ADD KEY `custom_field_8_value` (`custom_field_8_value`),
  ADD KEY `custom_field_9_value` (`custom_field_9_value`),
  ADD KEY `custom_field_10_value` (`custom_field_10_value`);

--
-- Indexes for table `tbl_pos_suppliers_taxes`
--
ALTER TABLE `tbl_pos_suppliers_taxes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_tax` (`supplier_id`,`name`,`percent`);

--
-- Indexes for table `tbl_pos_supplier_store_accounts`
--
ALTER TABLE `tbl_pos_supplier_store_accounts`
  ADD PRIMARY KEY (`sno`),
  ADD KEY `phppos_supplier_store_accounts_ibfk_1` (`receiving_id`),
  ADD KEY `phppos_supplier_store_accounts_ibfk_2` (`supplier_id`);

--
-- Indexes for table `tbl_pos_supplier_store_accounts_paid_receivings`
--
ALTER TABLE `tbl_pos_supplier_store_accounts_paid_receivings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_supplier_store_accounts_paid_receivings_ibfk_1` (`receiving_id`),
  ADD KEY `phppos_supplier_store_accounts_paid_receivings_ibfk_2` (`store_account_payment_receiving_id`);

--
-- Indexes for table `tbl_pos_tags`
--
ALTER TABLE `tbl_pos_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tag_name` (`name`),
  ADD KEY `deleted` (`deleted`);

--
-- Indexes for table `tbl_pos_tax_classes`
--
ALTER TABLE `tbl_pos_tax_classes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phppos_tax_classes_ibfk_1` (`location_id`);

--
-- Indexes for table `tbl_pos_tax_classes_taxes`
--
ALTER TABLE `tbl_pos_tax_classes_taxes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_tax` (`tax_class_id`,`name`,`percent`);

--
-- Indexes for table `tbl_pos_zips`
--
ALTER TABLE `tbl_pos_zips`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `phppos_zips_ibfk_1` (`shipping_zone_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_pos_access`
--
ALTER TABLE `tbl_pos_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_appointments`
--
ALTER TABLE `tbl_pos_appointments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_appointment_types`
--
ALTER TABLE `tbl_pos_appointment_types`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_app_files`
--
ALTER TABLE `tbl_pos_app_files`
  MODIFY `file_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_attributes`
--
ALTER TABLE `tbl_pos_attributes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_attribute_values`
--
ALTER TABLE `tbl_pos_attribute_values`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_categories`
--
ALTER TABLE `tbl_pos_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_currency_exchange_rates`
--
ALTER TABLE `tbl_pos_currency_exchange_rates`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_customers`
--
ALTER TABLE `tbl_pos_customers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_customers_series`
--
ALTER TABLE `tbl_pos_customers_series`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_customers_series_log`
--
ALTER TABLE `tbl_pos_customers_series_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_customers_taxes`
--
ALTER TABLE `tbl_pos_customers_taxes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_damaged_items_log`
--
ALTER TABLE `tbl_pos_damaged_items_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_employees`
--
ALTER TABLE `tbl_pos_employees`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_pos_employees_reset_password`
--
ALTER TABLE `tbl_pos_employees_reset_password`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_employees_time_clock`
--
ALTER TABLE `tbl_pos_employees_time_clock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_employees_time_off`
--
ALTER TABLE `tbl_pos_employees_time_off`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_employee_registers`
--
ALTER TABLE `tbl_pos_employee_registers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_expenses`
--
ALTER TABLE `tbl_pos_expenses`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_giftcards`
--
ALTER TABLE `tbl_pos_giftcards`
  MODIFY `giftcard_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_giftcards_log`
--
ALTER TABLE `tbl_pos_giftcards_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_grid_hidden_categories`
--
ALTER TABLE `tbl_pos_grid_hidden_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_grid_hidden_items`
--
ALTER TABLE `tbl_pos_grid_hidden_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_grid_hidden_item_kits`
--
ALTER TABLE `tbl_pos_grid_hidden_item_kits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_grid_hidden_tags`
--
ALTER TABLE `tbl_pos_grid_hidden_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_inventory`
--
ALTER TABLE `tbl_pos_inventory`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_inventory_counts`
--
ALTER TABLE `tbl_pos_inventory_counts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_inventory_counts_items`
--
ALTER TABLE `tbl_pos_inventory_counts_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_items`
--
ALTER TABLE `tbl_pos_items`
  MODIFY `item_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_items_pricing_history`
--
ALTER TABLE `tbl_pos_items_pricing_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_items_quantity_units`
--
ALTER TABLE `tbl_pos_items_quantity_units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_items_serial_numbers`
--
ALTER TABLE `tbl_pos_items_serial_numbers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_items_taxes`
--
ALTER TABLE `tbl_pos_items_taxes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_item_images`
--
ALTER TABLE `tbl_pos_item_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_item_kits`
--
ALTER TABLE `tbl_pos_item_kits`
  MODIFY `item_kit_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_item_kits_pricing_history`
--
ALTER TABLE `tbl_pos_item_kits_pricing_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_item_kits_taxes`
--
ALTER TABLE `tbl_pos_item_kits_taxes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_item_kit_images`
--
ALTER TABLE `tbl_pos_item_kit_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_item_kit_items`
--
ALTER TABLE `tbl_pos_item_kit_items`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_item_kit_item_kits`
--
ALTER TABLE `tbl_pos_item_kit_item_kits`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_item_variations`
--
ALTER TABLE `tbl_pos_item_variations`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_keys`
--
ALTER TABLE `tbl_pos_keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_limits`
--
ALTER TABLE `tbl_pos_limits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_locations`
--
ALTER TABLE `tbl_pos_locations`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_pos_location_items_taxes`
--
ALTER TABLE `tbl_pos_location_items_taxes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_location_item_kits_taxes`
--
ALTER TABLE `tbl_pos_location_item_kits_taxes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_logs`
--
ALTER TABLE `tbl_pos_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_manufacturers`
--
ALTER TABLE `tbl_pos_manufacturers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_messages`
--
ALTER TABLE `tbl_pos_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_message_receiver`
--
ALTER TABLE `tbl_pos_message_receiver`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_modifiers`
--
ALTER TABLE `tbl_pos_modifiers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_modifier_items`
--
ALTER TABLE `tbl_pos_modifier_items`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_people`
--
ALTER TABLE `tbl_pos_people`
  MODIFY `person_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_pos_people_files`
--
ALTER TABLE `tbl_pos_people_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_price_rules`
--
ALTER TABLE `tbl_pos_price_rules`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_price_rules_categories`
--
ALTER TABLE `tbl_pos_price_rules_categories`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_price_rules_items`
--
ALTER TABLE `tbl_pos_price_rules_items`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_price_rules_item_kits`
--
ALTER TABLE `tbl_pos_price_rules_item_kits`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_price_rules_locations`
--
ALTER TABLE `tbl_pos_price_rules_locations`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_price_rules_manufacturers`
--
ALTER TABLE `tbl_pos_price_rules_manufacturers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_price_rules_price_breaks`
--
ALTER TABLE `tbl_pos_price_rules_price_breaks`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_price_rules_tags`
--
ALTER TABLE `tbl_pos_price_rules_tags`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_price_tiers`
--
ALTER TABLE `tbl_pos_price_tiers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_receivings`
--
ALTER TABLE `tbl_pos_receivings`
  MODIFY `receiving_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_receivings_payments`
--
ALTER TABLE `tbl_pos_receivings_payments`
  MODIFY `payment_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_registers`
--
ALTER TABLE `tbl_pos_registers`
  MODIFY `register_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_pos_registers_cart`
--
ALTER TABLE `tbl_pos_registers_cart`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_register_currency_denominations`
--
ALTER TABLE `tbl_pos_register_currency_denominations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_pos_register_log`
--
ALTER TABLE `tbl_pos_register_log`
  MODIFY `register_log_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_register_log_audit`
--
ALTER TABLE `tbl_pos_register_log_audit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_register_log_denoms`
--
ALTER TABLE `tbl_pos_register_log_denoms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_register_log_payments`
--
ALTER TABLE `tbl_pos_register_log_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_sales`
--
ALTER TABLE `tbl_pos_sales`
  MODIFY `sale_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_sales_coupons`
--
ALTER TABLE `tbl_pos_sales_coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_sales_deliveries`
--
ALTER TABLE `tbl_pos_sales_deliveries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_sales_payments`
--
ALTER TABLE `tbl_pos_sales_payments`
  MODIFY `payment_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_sale_types`
--
ALTER TABLE `tbl_pos_sale_types`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_pos_shipping_methods`
--
ALTER TABLE `tbl_pos_shipping_methods`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_shipping_providers`
--
ALTER TABLE `tbl_pos_shipping_providers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_shipping_zones`
--
ALTER TABLE `tbl_pos_shipping_zones`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_store_accounts`
--
ALTER TABLE `tbl_pos_store_accounts`
  MODIFY `sno` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_store_accounts_paid_sales`
--
ALTER TABLE `tbl_pos_store_accounts_paid_sales`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_suppliers`
--
ALTER TABLE `tbl_pos_suppliers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_suppliers_taxes`
--
ALTER TABLE `tbl_pos_suppliers_taxes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_supplier_store_accounts`
--
ALTER TABLE `tbl_pos_supplier_store_accounts`
  MODIFY `sno` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_supplier_store_accounts_paid_receivings`
--
ALTER TABLE `tbl_pos_supplier_store_accounts_paid_receivings`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_tags`
--
ALTER TABLE `tbl_pos_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_pos_tax_classes`
--
ALTER TABLE `tbl_pos_tax_classes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_pos_tax_classes_taxes`
--
ALTER TABLE `tbl_pos_tax_classes_taxes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_pos_access`
--
ALTER TABLE `tbl_pos_access`
  ADD CONSTRAINT `phppos_access_key_fk` FOREIGN KEY (`key`) REFERENCES `tbl_pos_keys` (`key`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_pos_additional_item_numbers`
--
ALTER TABLE `tbl_pos_additional_item_numbers`
  ADD CONSTRAINT `tbl_pos_additional_item_numbers_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`),
  ADD CONSTRAINT `tbl_pos_additional_item_numbers_ibfk_2` FOREIGN KEY (`item_variation_id`) REFERENCES `tbl_pos_item_variations` (`id`);

--
-- Constraints for table `tbl_pos_appointments`
--
ALTER TABLE `tbl_pos_appointments`
  ADD CONSTRAINT `tbl_pos_appointments_ibfk_1` FOREIGN KEY (`appointments_type_id`) REFERENCES `tbl_pos_appointment_types` (`id`),
  ADD CONSTRAINT `tbl_pos_appointments_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `tbl_pos_people` (`person_id`),
  ADD CONSTRAINT `tbl_pos_appointments_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`),
  ADD CONSTRAINT `tbl_pos_appointments_ibfk_4` FOREIGN KEY (`employee_id`) REFERENCES `tbl_pos_employees` (`person_id`);

--
-- Constraints for table `tbl_pos_attributes`
--
ALTER TABLE `tbl_pos_attributes`
  ADD CONSTRAINT `tbl_pos_attributes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`);

--
-- Constraints for table `tbl_pos_attribute_values`
--
ALTER TABLE `tbl_pos_attribute_values`
  ADD CONSTRAINT `tbl_pos_attribute_values_ibfk_1` FOREIGN KEY (`attribute_id`) REFERENCES `tbl_pos_attributes` (`id`);

--
-- Constraints for table `tbl_pos_categories`
--
ALTER TABLE `tbl_pos_categories`
  ADD CONSTRAINT `tbl_pos_categories_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `tbl_pos_categories` (`id`),
  ADD CONSTRAINT `tbl_pos_categories_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `tbl_pos_app_files` (`file_id`);

--
-- Constraints for table `tbl_pos_customers`
--
ALTER TABLE `tbl_pos_customers`
  ADD CONSTRAINT `tbl_pos_customers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `tbl_pos_people` (`person_id`),
  ADD CONSTRAINT `tbl_pos_customers_ibfk_2` FOREIGN KEY (`tier_id`) REFERENCES `tbl_pos_price_tiers` (`id`),
  ADD CONSTRAINT `tbl_pos_customers_ibfk_3` FOREIGN KEY (`tax_class_id`) REFERENCES `tbl_pos_tax_classes` (`id`),
  ADD CONSTRAINT `tbl_pos_customers_ibfk_4` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_customers_series`
--
ALTER TABLE `tbl_pos_customers_series`
  ADD CONSTRAINT `tbl_pos_customers_series_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`),
  ADD CONSTRAINT `tbl_pos_customers_series_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `tbl_pos_people` (`person_id`),
  ADD CONSTRAINT `tbl_pos_customers_series_ibfk_3` FOREIGN KEY (`sale_id`) REFERENCES `tbl_pos_sales` (`sale_id`);

--
-- Constraints for table `tbl_pos_customers_series_log`
--
ALTER TABLE `tbl_pos_customers_series_log`
  ADD CONSTRAINT `tbl_pos_customers_series_log_ibfk_1` FOREIGN KEY (`series_id`) REFERENCES `tbl_pos_customers_series` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_pos_customers_taxes`
--
ALTER TABLE `tbl_pos_customers_taxes`
  ADD CONSTRAINT `tbl_pos_customers_taxes_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `tbl_pos_customers` (`person_id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_pos_damaged_items_log`
--
ALTER TABLE `tbl_pos_damaged_items_log`
  ADD CONSTRAINT `tbl_pos_damaged_items_log_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`),
  ADD CONSTRAINT `tbl_pos_damaged_items_log_ibfk_2` FOREIGN KEY (`item_variation_id`) REFERENCES `tbl_pos_item_variations` (`id`),
  ADD CONSTRAINT `tbl_pos_damaged_items_log_ibfk_3` FOREIGN KEY (`sale_id`) REFERENCES `tbl_pos_sales` (`sale_id`),
  ADD CONSTRAINT `tbl_pos_damaged_items_log_ibfk_4` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_ecommerce_locations`
--
ALTER TABLE `tbl_pos_ecommerce_locations`
  ADD CONSTRAINT `tbl_pos_ecommerce_locations_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_employees`
--
ALTER TABLE `tbl_pos_employees`
  ADD CONSTRAINT `tbl_pos_employees_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `tbl_pos_people` (`person_id`);

--
-- Constraints for table `tbl_pos_employees_app_config`
--
ALTER TABLE `tbl_pos_employees_app_config`
  ADD CONSTRAINT `tbl_pos_employees_app_config_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_pos_employees` (`person_id`);

--
-- Constraints for table `tbl_pos_employees_locations`
--
ALTER TABLE `tbl_pos_employees_locations`
  ADD CONSTRAINT `tbl_pos_employees_locations_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_pos_employees` (`person_id`),
  ADD CONSTRAINT `tbl_pos_employees_locations_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_employees_reset_password`
--
ALTER TABLE `tbl_pos_employees_reset_password`
  ADD CONSTRAINT `tbl_pos_employees_reset_password_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_pos_employees` (`person_id`);

--
-- Constraints for table `tbl_pos_employees_time_clock`
--
ALTER TABLE `tbl_pos_employees_time_clock`
  ADD CONSTRAINT `tbl_pos_employees_time_clock_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_pos_employees` (`person_id`),
  ADD CONSTRAINT `tbl_pos_employees_time_clock_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_employees_time_off`
--
ALTER TABLE `tbl_pos_employees_time_off`
  ADD CONSTRAINT `tbl_pos_employees_time_off_ibfk_1` FOREIGN KEY (`employee_requested_person_id`) REFERENCES `tbl_pos_people` (`person_id`),
  ADD CONSTRAINT `tbl_pos_employees_time_off_ibfk_2` FOREIGN KEY (`employee_approved_person_id`) REFERENCES `tbl_pos_people` (`person_id`),
  ADD CONSTRAINT `tbl_pos_employees_time_off_ibfk_3` FOREIGN KEY (`employee_requested_location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_employee_registers`
--
ALTER TABLE `tbl_pos_employee_registers`
  ADD CONSTRAINT `tbl_pos_employee_registers_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_pos_employees` (`person_id`),
  ADD CONSTRAINT `tbl_pos_employee_registers_ibfk_2` FOREIGN KEY (`register_id`) REFERENCES `tbl_pos_registers` (`register_id`);

--
-- Constraints for table `tbl_pos_expenses`
--
ALTER TABLE `tbl_pos_expenses`
  ADD CONSTRAINT `tbl_pos_expenses_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`),
  ADD CONSTRAINT `tbl_pos_expenses_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `tbl_pos_employees` (`person_id`),
  ADD CONSTRAINT `tbl_pos_expenses_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `tbl_pos_categories` (`id`),
  ADD CONSTRAINT `tbl_pos_expenses_ibfk_4` FOREIGN KEY (`approved_employee_id`) REFERENCES `tbl_pos_employees` (`person_id`);

--
-- Constraints for table `tbl_pos_giftcards`
--
ALTER TABLE `tbl_pos_giftcards`
  ADD CONSTRAINT `tbl_pos_giftcards_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `tbl_pos_customers` (`person_id`);

--
-- Constraints for table `tbl_pos_giftcards_log`
--
ALTER TABLE `tbl_pos_giftcards_log`
  ADD CONSTRAINT `tbl_pos_giftcards_log_ibfk_1` FOREIGN KEY (`giftcard_id`) REFERENCES `tbl_pos_giftcards` (`giftcard_id`);

--
-- Constraints for table `tbl_pos_grid_hidden_categories`
--
ALTER TABLE `tbl_pos_grid_hidden_categories`
  ADD CONSTRAINT `tbl_pos_grid_hidden_categories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `tbl_pos_categories` (`id`),
  ADD CONSTRAINT `tbl_pos_grid_hidden_categories_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_grid_hidden_items`
--
ALTER TABLE `tbl_pos_grid_hidden_items`
  ADD CONSTRAINT `tbl_pos_grid_hidden_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`),
  ADD CONSTRAINT `tbl_pos_grid_hidden_items_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_grid_hidden_item_kits`
--
ALTER TABLE `tbl_pos_grid_hidden_item_kits`
  ADD CONSTRAINT `tbl_pos_grid_hidden_item_kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`),
  ADD CONSTRAINT `tbl_pos_grid_hidden_item_kits_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_grid_hidden_tags`
--
ALTER TABLE `tbl_pos_grid_hidden_tags`
  ADD CONSTRAINT `tbl_pos_grid_hidden_tags_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `tbl_pos_tags` (`id`),
  ADD CONSTRAINT `tbl_pos_grid_hidden_tags_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_inventory`
--
ALTER TABLE `tbl_pos_inventory`
  ADD CONSTRAINT `tbl_pos_inventory_ibfk_1` FOREIGN KEY (`trans_items`) REFERENCES `tbl_pos_items` (`item_id`),
  ADD CONSTRAINT `tbl_pos_inventory_ibfk_2` FOREIGN KEY (`trans_user`) REFERENCES `tbl_pos_employees` (`person_id`),
  ADD CONSTRAINT `tbl_pos_inventory_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`),
  ADD CONSTRAINT `tbl_pos_inventory_ibfk_4` FOREIGN KEY (`item_variation_id`) REFERENCES `tbl_pos_item_variations` (`id`);

--
-- Constraints for table `tbl_pos_inventory_counts`
--
ALTER TABLE `tbl_pos_inventory_counts`
  ADD CONSTRAINT `tbl_pos_inventory_counts_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_pos_employees` (`person_id`),
  ADD CONSTRAINT `tbl_pos_inventory_counts_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_inventory_counts_items`
--
ALTER TABLE `tbl_pos_inventory_counts_items`
  ADD CONSTRAINT `inventory_counts_items_ibfk_3` FOREIGN KEY (`item_variation_id`) REFERENCES `tbl_pos_item_variations` (`id`),
  ADD CONSTRAINT `tbl_pos_inventory_counts_items_ibfk_1` FOREIGN KEY (`inventory_counts_id`) REFERENCES `tbl_pos_inventory_counts` (`id`),
  ADD CONSTRAINT `tbl_pos_inventory_counts_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`);

--
-- Constraints for table `tbl_pos_items`
--
ALTER TABLE `tbl_pos_items`
  ADD CONSTRAINT `tbl_pos_items_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `tbl_pos_suppliers` (`person_id`),
  ADD CONSTRAINT `tbl_pos_items_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `tbl_pos_categories` (`id`),
  ADD CONSTRAINT `tbl_pos_items_ibfk_4` FOREIGN KEY (`manufacturer_id`) REFERENCES `tbl_pos_manufacturers` (`id`),
  ADD CONSTRAINT `tbl_pos_items_ibfk_6` FOREIGN KEY (`tax_class_id`) REFERENCES `tbl_pos_tax_classes` (`id`),
  ADD CONSTRAINT `tbl_pos_items_ibfk_7` FOREIGN KEY (`main_image_id`) REFERENCES `tbl_pos_item_images` (`image_id`);

--
-- Constraints for table `tbl_pos_items_modifiers`
--
ALTER TABLE `tbl_pos_items_modifiers`
  ADD CONSTRAINT `tbl_pos_items_modifiers_ibfk_1` FOREIGN KEY (`modifier_id`) REFERENCES `tbl_pos_modifiers` (`id`),
  ADD CONSTRAINT `tbl_pos_items_modifiers_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`);

--
-- Constraints for table `tbl_pos_items_pricing_history`
--
ALTER TABLE `tbl_pos_items_pricing_history`
  ADD CONSTRAINT `tbl_pos_items_pricing_history_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`),
  ADD CONSTRAINT `tbl_pos_items_pricing_history_ibfk_2` FOREIGN KEY (`item_variation_id`) REFERENCES `tbl_pos_item_variations` (`id`),
  ADD CONSTRAINT `tbl_pos_items_pricing_history_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`),
  ADD CONSTRAINT `tbl_pos_items_pricing_history_ibfk_4` FOREIGN KEY (`employee_id`) REFERENCES `tbl_pos_employees` (`person_id`);

--
-- Constraints for table `tbl_pos_items_quantity_units`
--
ALTER TABLE `tbl_pos_items_quantity_units`
  ADD CONSTRAINT `tbl_pos_items_quantity_units_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`);

--
-- Constraints for table `tbl_pos_items_serial_numbers`
--
ALTER TABLE `tbl_pos_items_serial_numbers`
  ADD CONSTRAINT `tbl_pos_items_serial_numbers_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`);

--
-- Constraints for table `tbl_pos_items_tags`
--
ALTER TABLE `tbl_pos_items_tags`
  ADD CONSTRAINT `tbl_pos_items_tags_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`),
  ADD CONSTRAINT `tbl_pos_items_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tbl_pos_tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_pos_items_taxes`
--
ALTER TABLE `tbl_pos_items_taxes`
  ADD CONSTRAINT `tbl_pos_items_taxes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_pos_items_tier_prices`
--
ALTER TABLE `tbl_pos_items_tier_prices`
  ADD CONSTRAINT `tbl_pos_items_tier_prices_ibfk_1` FOREIGN KEY (`tier_id`) REFERENCES `tbl_pos_price_tiers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pos_items_tier_prices_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`);

--
-- Constraints for table `tbl_pos_item_attributes`
--
ALTER TABLE `tbl_pos_item_attributes`
  ADD CONSTRAINT `tbl_pos_item_attributes_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`),
  ADD CONSTRAINT `tbl_pos_item_attributes_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `tbl_pos_attributes` (`id`);

--
-- Constraints for table `tbl_pos_item_attribute_values`
--
ALTER TABLE `tbl_pos_item_attribute_values`
  ADD CONSTRAINT `tbl_pos_item_attribute_values_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`),
  ADD CONSTRAINT `tbl_pos_item_attribute_values_ibfk_2` FOREIGN KEY (`attribute_value_id`) REFERENCES `tbl_pos_attribute_values` (`id`);

--
-- Constraints for table `tbl_pos_item_images`
--
ALTER TABLE `tbl_pos_item_images`
  ADD CONSTRAINT `tbl_pos_item_images_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`),
  ADD CONSTRAINT `tbl_pos_item_images_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `tbl_pos_app_files` (`file_id`),
  ADD CONSTRAINT `tbl_pos_item_images_ibfk_3` FOREIGN KEY (`item_variation_id`) REFERENCES `tbl_pos_item_variations` (`id`);

--
-- Constraints for table `tbl_pos_item_kits`
--
ALTER TABLE `tbl_pos_item_kits`
  ADD CONSTRAINT `tbl_pos_item_kits_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `tbl_pos_categories` (`id`),
  ADD CONSTRAINT `tbl_pos_item_kits_ibfk_2` FOREIGN KEY (`manufacturer_id`) REFERENCES `tbl_pos_manufacturers` (`id`),
  ADD CONSTRAINT `tbl_pos_item_kits_ibfk_3` FOREIGN KEY (`tax_class_id`) REFERENCES `tbl_pos_tax_classes` (`id`),
  ADD CONSTRAINT `tbl_pos_item_kits_ibfk_4` FOREIGN KEY (`main_image_id`) REFERENCES `tbl_pos_item_kit_images` (`image_id`);

--
-- Constraints for table `tbl_pos_item_kits_modifiers`
--
ALTER TABLE `tbl_pos_item_kits_modifiers`
  ADD CONSTRAINT `tbl_pos_item_kits_modifiers_ibfk_1` FOREIGN KEY (`modifier_id`) REFERENCES `tbl_pos_modifiers` (`id`),
  ADD CONSTRAINT `tbl_pos_item_kits_modifiers_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`);

--
-- Constraints for table `tbl_pos_item_kits_pricing_history`
--
ALTER TABLE `tbl_pos_item_kits_pricing_history`
  ADD CONSTRAINT `tbl_pos_item_kits_pricing_history_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`),
  ADD CONSTRAINT `tbl_pos_item_kits_pricing_history_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`),
  ADD CONSTRAINT `tbl_pos_item_kits_pricing_history_ibfk_3` FOREIGN KEY (`employee_id`) REFERENCES `tbl_pos_employees` (`person_id`);

--
-- Constraints for table `tbl_pos_item_kits_tags`
--
ALTER TABLE `tbl_pos_item_kits_tags`
  ADD CONSTRAINT `tbl_pos_item_kits_tags_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`),
  ADD CONSTRAINT `tbl_pos_item_kits_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tbl_pos_tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_pos_item_kits_taxes`
--
ALTER TABLE `tbl_pos_item_kits_taxes`
  ADD CONSTRAINT `tbl_pos_item_kits_taxes_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_pos_item_kits_tier_prices`
--
ALTER TABLE `tbl_pos_item_kits_tier_prices`
  ADD CONSTRAINT `tbl_pos_item_kits_tier_prices_ibfk_1` FOREIGN KEY (`tier_id`) REFERENCES `tbl_pos_price_tiers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pos_item_kits_tier_prices_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`);

--
-- Constraints for table `tbl_pos_item_kit_images`
--
ALTER TABLE `tbl_pos_item_kit_images`
  ADD CONSTRAINT `tbl_pos_item_kit_images_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`),
  ADD CONSTRAINT `tbl_pos_item_kit_images_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `tbl_pos_app_files` (`file_id`);

--
-- Constraints for table `tbl_pos_item_kit_items`
--
ALTER TABLE `tbl_pos_item_kit_items`
  ADD CONSTRAINT `tbl_pos_item_kit_items_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pos_item_kit_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pos_item_kit_items_ibfk_3` FOREIGN KEY (`item_variation_id`) REFERENCES `tbl_pos_item_variations` (`id`);

--
-- Constraints for table `tbl_pos_item_kit_item_kits`
--
ALTER TABLE `tbl_pos_item_kit_item_kits`
  ADD CONSTRAINT `tbl_pos_item_kit_item_kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pos_item_kit_item_kits_ibfk_2` FOREIGN KEY (`item_kit_item_kit`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_pos_item_variations`
--
ALTER TABLE `tbl_pos_item_variations`
  ADD CONSTRAINT `tbl_pos_item_variations_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`);

--
-- Constraints for table `tbl_pos_item_variation_attribute_values`
--
ALTER TABLE `tbl_pos_item_variation_attribute_values`
  ADD CONSTRAINT `tbl_pos_item_variation_attribute_values_ibfk_1` FOREIGN KEY (`attribute_value_id`) REFERENCES `tbl_pos_attribute_values` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pos_item_variation_attribute_values_ibfk_2` FOREIGN KEY (`item_variation_id`) REFERENCES `tbl_pos_item_variations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_pos_keys`
--
ALTER TABLE `tbl_pos_keys`
  ADD CONSTRAINT `phppos_keys_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `tbl_pos_employees` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_pos_limits`
--
ALTER TABLE `tbl_pos_limits`
  ADD CONSTRAINT `phppos_limits_api_key_fk` FOREIGN KEY (`api_key`) REFERENCES `tbl_pos_keys` (`key`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_pos_locations`
--
ALTER TABLE `tbl_pos_locations`
  ADD CONSTRAINT `tbl_pos_locations_ibfk_1` FOREIGN KEY (`company_logo`) REFERENCES `tbl_pos_app_files` (`file_id`),
  ADD CONSTRAINT `tbl_pos_locations_ibfk_2` FOREIGN KEY (`tax_class_id`) REFERENCES `tbl_pos_tax_classes` (`id`);

--
-- Constraints for table `tbl_pos_location_items`
--
ALTER TABLE `tbl_pos_location_items`
  ADD CONSTRAINT `tbl_pos_location_items_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`),
  ADD CONSTRAINT `tbl_pos_location_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`),
  ADD CONSTRAINT `tbl_pos_location_items_ibfk_3` FOREIGN KEY (`tax_class_id`) REFERENCES `tbl_pos_tax_classes` (`id`);

--
-- Constraints for table `tbl_pos_location_items_taxes`
--
ALTER TABLE `tbl_pos_location_items_taxes`
  ADD CONSTRAINT `tbl_pos_location_items_taxes_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pos_location_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_pos_location_items_tier_prices`
--
ALTER TABLE `tbl_pos_location_items_tier_prices`
  ADD CONSTRAINT `tbl_pos_location_items_tier_prices_ibfk_1` FOREIGN KEY (`tier_id`) REFERENCES `tbl_pos_price_tiers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pos_location_items_tier_prices_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`),
  ADD CONSTRAINT `tbl_pos_location_items_tier_prices_ibfk_3` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`);

--
-- Constraints for table `tbl_pos_location_item_kits`
--
ALTER TABLE `tbl_pos_location_item_kits`
  ADD CONSTRAINT `tbl_pos_location_item_kits_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`),
  ADD CONSTRAINT `tbl_pos_location_item_kits_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`),
  ADD CONSTRAINT `tbl_pos_location_item_kits_ibfk_3` FOREIGN KEY (`tax_class_id`) REFERENCES `tbl_pos_tax_classes` (`id`);

--
-- Constraints for table `tbl_pos_location_item_kits_taxes`
--
ALTER TABLE `tbl_pos_location_item_kits_taxes`
  ADD CONSTRAINT `tbl_pos_location_item_kits_taxes_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pos_location_item_kits_taxes_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_pos_location_item_kits_tier_prices`
--
ALTER TABLE `tbl_pos_location_item_kits_tier_prices`
  ADD CONSTRAINT `tbl_pos_location_item_kits_tier_prices_ibfk_1` FOREIGN KEY (`tier_id`) REFERENCES `tbl_pos_price_tiers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_pos_location_item_kits_tier_prices_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`),
  ADD CONSTRAINT `tbl_pos_location_item_kits_tier_prices_ibfk_3` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`);

--
-- Constraints for table `tbl_pos_location_item_variations`
--
ALTER TABLE `tbl_pos_location_item_variations`
  ADD CONSTRAINT `phppos_item_attribute_location_values_ibfk_1` FOREIGN KEY (`item_variation_id`) REFERENCES `tbl_pos_item_variations` (`id`),
  ADD CONSTRAINT `phppos_item_attribute_location_values_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_messages`
--
ALTER TABLE `tbl_pos_messages`
  ADD CONSTRAINT `tbl_pos_messages_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `tbl_pos_employees` (`person_id`);

--
-- Constraints for table `tbl_pos_message_receiver`
--
ALTER TABLE `tbl_pos_message_receiver`
  ADD CONSTRAINT `tbl_pos_message_receiver_ibfk_1` FOREIGN KEY (`message_id`) REFERENCES `tbl_pos_messages` (`id`),
  ADD CONSTRAINT `tbl_pos_message_receiver_ibfk_2` FOREIGN KEY (`receiver_id`) REFERENCES `tbl_pos_employees` (`person_id`);

--
-- Constraints for table `tbl_pos_modifier_items`
--
ALTER TABLE `tbl_pos_modifier_items`
  ADD CONSTRAINT `tbl_pos_modifier_items_ibfk_1` FOREIGN KEY (`modifier_id`) REFERENCES `tbl_pos_modifiers` (`id`);

--
-- Constraints for table `tbl_pos_modules_actions`
--
ALTER TABLE `tbl_pos_modules_actions`
  ADD CONSTRAINT `tbl_pos_modules_actions_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `tbl_pos_modules` (`module_id`);

--
-- Constraints for table `tbl_pos_people`
--
ALTER TABLE `tbl_pos_people`
  ADD CONSTRAINT `tbl_pos_people_ibfk_1` FOREIGN KEY (`image_id`) REFERENCES `tbl_pos_app_files` (`file_id`);

--
-- Constraints for table `tbl_pos_people_files`
--
ALTER TABLE `tbl_pos_people_files`
  ADD CONSTRAINT `tbl_pos_people_files_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `tbl_pos_app_files` (`file_id`);

--
-- Constraints for table `tbl_pos_permissions_actions_locations`
--
ALTER TABLE `tbl_pos_permissions_actions_locations`
  ADD CONSTRAINT `tbl_pos_permissions_actions_locations_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `tbl_pos_modules` (`module_id`),
  ADD CONSTRAINT `tbl_pos_permissions_actions_locations_ibfk_2` FOREIGN KEY (`person_id`) REFERENCES `tbl_pos_employees` (`person_id`),
  ADD CONSTRAINT `tbl_pos_permissions_actions_locations_ibfk_3` FOREIGN KEY (`action_id`) REFERENCES `tbl_pos_modules_actions` (`action_id`),
  ADD CONSTRAINT `tbl_pos_permissions_actions_locations_ibfk_4` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_permissions_locations`
--
ALTER TABLE `tbl_pos_permissions_locations`
  ADD CONSTRAINT `tbl_pos_permissions_locations_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `tbl_pos_employees` (`person_id`),
  ADD CONSTRAINT `tbl_pos_permissions_locations_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `tbl_pos_modules` (`module_id`),
  ADD CONSTRAINT `tbl_pos_permissions_locations_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_price_rules_categories`
--
ALTER TABLE `tbl_pos_price_rules_categories`
  ADD CONSTRAINT `tbl_pos_price_rules_categories_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `tbl_pos_price_rules` (`id`),
  ADD CONSTRAINT `tbl_pos_price_rules_categories_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `tbl_pos_categories` (`id`);

--
-- Constraints for table `tbl_pos_price_rules_items`
--
ALTER TABLE `tbl_pos_price_rules_items`
  ADD CONSTRAINT `tbl_pos_price_rules_items_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `tbl_pos_price_rules` (`id`),
  ADD CONSTRAINT `tbl_pos_price_rules_items_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`);

--
-- Constraints for table `tbl_pos_price_rules_item_kits`
--
ALTER TABLE `tbl_pos_price_rules_item_kits`
  ADD CONSTRAINT `tbl_pos_price_rules_item_kits_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `tbl_pos_price_rules` (`id`),
  ADD CONSTRAINT `tbl_pos_price_rules_item_kits_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`);

--
-- Constraints for table `tbl_pos_price_rules_locations`
--
ALTER TABLE `tbl_pos_price_rules_locations`
  ADD CONSTRAINT `tbl_pos_price_rules_locations_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `tbl_pos_price_rules` (`id`),
  ADD CONSTRAINT `tbl_pos_price_rules_locations_ibfk_2` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_price_rules_manufacturers`
--
ALTER TABLE `tbl_pos_price_rules_manufacturers`
  ADD CONSTRAINT `tbl_pos_price_rules_manufacturers_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `tbl_pos_price_rules` (`id`),
  ADD CONSTRAINT `tbl_pos_price_rules_manufacturers_ibfk_2` FOREIGN KEY (`manufacturer_id`) REFERENCES `tbl_pos_manufacturers` (`id`);

--
-- Constraints for table `tbl_pos_price_rules_price_breaks`
--
ALTER TABLE `tbl_pos_price_rules_price_breaks`
  ADD CONSTRAINT `tbl_pos_price_rules_price_breaks_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `tbl_pos_price_rules` (`id`);

--
-- Constraints for table `tbl_pos_price_rules_tags`
--
ALTER TABLE `tbl_pos_price_rules_tags`
  ADD CONSTRAINT `tbl_pos_price_rules_tags_ibfk_1` FOREIGN KEY (`rule_id`) REFERENCES `tbl_pos_price_rules` (`id`),
  ADD CONSTRAINT `tbl_pos_price_rules_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tbl_pos_tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_pos_receivings`
--
ALTER TABLE `tbl_pos_receivings`
  ADD CONSTRAINT `tbl_pos_receivings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_pos_employees` (`person_id`),
  ADD CONSTRAINT `tbl_pos_receivings_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `tbl_pos_suppliers` (`person_id`),
  ADD CONSTRAINT `tbl_pos_receivings_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`),
  ADD CONSTRAINT `tbl_pos_receivings_ibfk_4` FOREIGN KEY (`transfer_to_location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_receivings_items`
--
ALTER TABLE `tbl_pos_receivings_items`
  ADD CONSTRAINT `tbl_pos_receivings_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`),
  ADD CONSTRAINT `tbl_pos_receivings_items_ibfk_2` FOREIGN KEY (`receiving_id`) REFERENCES `tbl_pos_receivings` (`receiving_id`),
  ADD CONSTRAINT `tbl_pos_receivings_items_ibfk_3` FOREIGN KEY (`item_variation_id`) REFERENCES `tbl_pos_item_variations` (`id`),
  ADD CONSTRAINT `tbl_pos_receivings_items_ibfk_4` FOREIGN KEY (`items_quantity_units_id`) REFERENCES `tbl_pos_items_quantity_units` (`id`);

--
-- Constraints for table `tbl_pos_receivings_items_taxes`
--
ALTER TABLE `tbl_pos_receivings_items_taxes`
  ADD CONSTRAINT `tbl_pos_receivings_items_taxes_ibfk_1` FOREIGN KEY (`receiving_id`) REFERENCES `tbl_pos_receivings` (`receiving_id`),
  ADD CONSTRAINT `tbl_pos_receivings_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`);

--
-- Constraints for table `tbl_pos_receivings_payments`
--
ALTER TABLE `tbl_pos_receivings_payments`
  ADD CONSTRAINT `tbl_pos_receivings_payments_ibfk_1` FOREIGN KEY (`receiving_id`) REFERENCES `tbl_pos_receivings` (`receiving_id`);

--
-- Constraints for table `tbl_pos_registers`
--
ALTER TABLE `tbl_pos_registers`
  ADD CONSTRAINT `tbl_pos_registers_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_registers_cart`
--
ALTER TABLE `tbl_pos_registers_cart`
  ADD CONSTRAINT `tbl_pos_registers_cart_ibfk_1` FOREIGN KEY (`register_id`) REFERENCES `tbl_pos_registers` (`register_id`);

--
-- Constraints for table `tbl_pos_register_log`
--
ALTER TABLE `tbl_pos_register_log`
  ADD CONSTRAINT `tbl_pos_register_log_ibfk_1` FOREIGN KEY (`employee_id_open`) REFERENCES `tbl_pos_employees` (`person_id`),
  ADD CONSTRAINT `tbl_pos_register_log_ibfk_2` FOREIGN KEY (`register_id`) REFERENCES `tbl_pos_registers` (`register_id`),
  ADD CONSTRAINT `tbl_pos_register_log_ibfk_3` FOREIGN KEY (`employee_id_close`) REFERENCES `tbl_pos_employees` (`person_id`);

--
-- Constraints for table `tbl_pos_register_log_audit`
--
ALTER TABLE `tbl_pos_register_log_audit`
  ADD CONSTRAINT `register_log_audit_ibfk_1` FOREIGN KEY (`register_log_id`) REFERENCES `tbl_pos_register_log` (`register_log_id`),
  ADD CONSTRAINT `register_log_audit_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `tbl_pos_employees` (`person_id`);

--
-- Constraints for table `tbl_pos_register_log_denoms`
--
ALTER TABLE `tbl_pos_register_log_denoms`
  ADD CONSTRAINT `tbl_pos_register_log_denoms_ibfk_1` FOREIGN KEY (`register_log_id`) REFERENCES `tbl_pos_register_log` (`register_log_id`),
  ADD CONSTRAINT `tbl_pos_register_log_denoms_ibfk_2` FOREIGN KEY (`register_currency_denominations_id`) REFERENCES `tbl_pos_register_currency_denominations` (`id`);

--
-- Constraints for table `tbl_pos_register_log_payments`
--
ALTER TABLE `tbl_pos_register_log_payments`
  ADD CONSTRAINT `tbl_pos_register_log_payments_ibfk_1` FOREIGN KEY (`register_log_id`) REFERENCES `tbl_pos_register_log` (`register_log_id`);

--
-- Constraints for table `tbl_pos_sales`
--
ALTER TABLE `tbl_pos_sales`
  ADD CONSTRAINT `tbl_pos_sales_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `tbl_pos_employees` (`person_id`),
  ADD CONSTRAINT `tbl_pos_sales_ibfk_10` FOREIGN KEY (`suspended`) REFERENCES `tbl_pos_sale_types` (`id`),
  ADD CONSTRAINT `tbl_pos_sales_ibfk_11` FOREIGN KEY (`return_sale_id`) REFERENCES `tbl_pos_sales` (`sale_id`),
  ADD CONSTRAINT `tbl_pos_sales_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `tbl_pos_customers` (`person_id`),
  ADD CONSTRAINT `tbl_pos_sales_ibfk_3` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`),
  ADD CONSTRAINT `tbl_pos_sales_ibfk_4` FOREIGN KEY (`deleted_by`) REFERENCES `tbl_pos_employees` (`person_id`),
  ADD CONSTRAINT `tbl_pos_sales_ibfk_5` FOREIGN KEY (`tier_id`) REFERENCES `tbl_pos_price_tiers` (`id`),
  ADD CONSTRAINT `tbl_pos_sales_ibfk_6` FOREIGN KEY (`sold_by_employee_id`) REFERENCES `tbl_pos_employees` (`person_id`),
  ADD CONSTRAINT `tbl_pos_sales_ibfk_7` FOREIGN KEY (`register_id`) REFERENCES `tbl_pos_registers` (`register_id`),
  ADD CONSTRAINT `tbl_pos_sales_ibfk_8` FOREIGN KEY (`signature_image_id`) REFERENCES `tbl_pos_app_files` (`file_id`),
  ADD CONSTRAINT `tbl_pos_sales_ibfk_9` FOREIGN KEY (`rule_id`) REFERENCES `tbl_pos_price_rules` (`id`);

--
-- Constraints for table `tbl_pos_sales_coupons`
--
ALTER TABLE `tbl_pos_sales_coupons`
  ADD CONSTRAINT `tbl_pos_sales_coupons_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `tbl_pos_sales` (`sale_id`),
  ADD CONSTRAINT `tbl_pos_sales_coupons_ibfk_2` FOREIGN KEY (`rule_id`) REFERENCES `tbl_pos_price_rules` (`id`);

--
-- Constraints for table `tbl_pos_sales_deliveries`
--
ALTER TABLE `tbl_pos_sales_deliveries`
  ADD CONSTRAINT `tbl_pos_sales_deliveries_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `tbl_pos_sales` (`sale_id`),
  ADD CONSTRAINT `tbl_pos_sales_deliveries_ibfk_2` FOREIGN KEY (`shipping_address_person_id`) REFERENCES `tbl_pos_people` (`person_id`),
  ADD CONSTRAINT `tbl_pos_sales_deliveries_ibfk_3` FOREIGN KEY (`shipping_method_id`) REFERENCES `tbl_pos_shipping_methods` (`id`),
  ADD CONSTRAINT `tbl_pos_sales_deliveries_ibfk_4` FOREIGN KEY (`shipping_zone_id`) REFERENCES `tbl_pos_shipping_zones` (`id`),
  ADD CONSTRAINT `tbl_pos_sales_deliveries_ibfk_5` FOREIGN KEY (`tax_class_id`) REFERENCES `tbl_pos_tax_classes` (`id`),
  ADD CONSTRAINT `tbl_pos_sales_deliveries_ibfk_6` FOREIGN KEY (`delivery_employee_person_id`) REFERENCES `tbl_pos_employees` (`person_id`);

--
-- Constraints for table `tbl_pos_sales_items`
--
ALTER TABLE `tbl_pos_sales_items`
  ADD CONSTRAINT `tbl_pos_sales_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`),
  ADD CONSTRAINT `tbl_pos_sales_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `tbl_pos_sales` (`sale_id`),
  ADD CONSTRAINT `tbl_pos_sales_items_ibfk_3` FOREIGN KEY (`rule_id`) REFERENCES `tbl_pos_price_rules` (`id`),
  ADD CONSTRAINT `tbl_pos_sales_items_ibfk_4` FOREIGN KEY (`item_variation_id`) REFERENCES `tbl_pos_item_variations` (`id`),
  ADD CONSTRAINT `tbl_pos_sales_items_ibfk_5` FOREIGN KEY (`series_id`) REFERENCES `tbl_pos_customers_series` (`id`),
  ADD CONSTRAINT `tbl_pos_sales_items_ibfk_6` FOREIGN KEY (`items_quantity_units_id`) REFERENCES `tbl_pos_items_quantity_units` (`id`);

--
-- Constraints for table `tbl_pos_sales_items_modifier_items`
--
ALTER TABLE `tbl_pos_sales_items_modifier_items`
  ADD CONSTRAINT `tbl_pos_sales_items_modifier_items_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`),
  ADD CONSTRAINT `tbl_pos_sales_items_modifier_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `tbl_pos_sales` (`sale_id`),
  ADD CONSTRAINT `tbl_pos_sales_items_modifier_items_ibfk_3` FOREIGN KEY (`modifier_item_id`) REFERENCES `tbl_pos_modifier_items` (`id`);

--
-- Constraints for table `tbl_pos_sales_items_taxes`
--
ALTER TABLE `tbl_pos_sales_items_taxes`
  ADD CONSTRAINT `tbl_pos_sales_items_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `tbl_pos_sales_items` (`sale_id`),
  ADD CONSTRAINT `tbl_pos_sales_items_taxes_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `tbl_pos_items` (`item_id`);

--
-- Constraints for table `tbl_pos_sales_item_kits`
--
ALTER TABLE `tbl_pos_sales_item_kits`
  ADD CONSTRAINT `tbl_pos_sales_item_kits_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`),
  ADD CONSTRAINT `tbl_pos_sales_item_kits_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `tbl_pos_sales` (`sale_id`),
  ADD CONSTRAINT `tbl_pos_sales_item_kits_ibfk_3` FOREIGN KEY (`rule_id`) REFERENCES `tbl_pos_price_rules` (`id`);

--
-- Constraints for table `tbl_pos_sales_item_kits_modifier_items`
--
ALTER TABLE `tbl_pos_sales_item_kits_modifier_items`
  ADD CONSTRAINT `tbl_pos_sales_item_kits_modifier_items_ibfk_1` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`),
  ADD CONSTRAINT `tbl_pos_sales_item_kits_modifier_items_ibfk_2` FOREIGN KEY (`sale_id`) REFERENCES `tbl_pos_sales` (`sale_id`),
  ADD CONSTRAINT `tbl_pos_sales_item_kits_modifier_items_ibfk_3` FOREIGN KEY (`modifier_item_id`) REFERENCES `tbl_pos_modifier_items` (`id`);

--
-- Constraints for table `tbl_pos_sales_item_kits_taxes`
--
ALTER TABLE `tbl_pos_sales_item_kits_taxes`
  ADD CONSTRAINT `tbl_pos_sales_item_kits_taxes_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `tbl_pos_sales_item_kits` (`sale_id`),
  ADD CONSTRAINT `tbl_pos_sales_item_kits_taxes_ibfk_2` FOREIGN KEY (`item_kit_id`) REFERENCES `tbl_pos_item_kits` (`item_kit_id`);

--
-- Constraints for table `tbl_pos_sales_payments`
--
ALTER TABLE `tbl_pos_sales_payments`
  ADD CONSTRAINT `tbl_pos_sales_payments_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `tbl_pos_sales` (`sale_id`);

--
-- Constraints for table `tbl_pos_shipping_methods`
--
ALTER TABLE `tbl_pos_shipping_methods`
  ADD CONSTRAINT `tbl_pos_shipping_methods_ibfk_1` FOREIGN KEY (`shipping_provider_id`) REFERENCES `tbl_pos_shipping_providers` (`id`),
  ADD CONSTRAINT `tbl_pos_shipping_methods_ibfk_2` FOREIGN KEY (`fee_tax_class_id`) REFERENCES `tbl_pos_tax_classes` (`id`);

--
-- Constraints for table `tbl_pos_shipping_zones`
--
ALTER TABLE `tbl_pos_shipping_zones`
  ADD CONSTRAINT `tbl_pos_shipping_zones_ibfk_1` FOREIGN KEY (`tax_class_id`) REFERENCES `tbl_pos_tax_classes` (`id`);

--
-- Constraints for table `tbl_pos_store_accounts`
--
ALTER TABLE `tbl_pos_store_accounts`
  ADD CONSTRAINT `tbl_pos_store_accounts_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `tbl_pos_sales` (`sale_id`),
  ADD CONSTRAINT `tbl_pos_store_accounts_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `tbl_pos_customers` (`person_id`);

--
-- Constraints for table `tbl_pos_store_accounts_paid_sales`
--
ALTER TABLE `tbl_pos_store_accounts_paid_sales`
  ADD CONSTRAINT `phppos_store_accounts_sales_ibfk_1` FOREIGN KEY (`sale_id`) REFERENCES `tbl_pos_sales` (`sale_id`),
  ADD CONSTRAINT `phppos_store_accounts_sales_ibfk_2` FOREIGN KEY (`store_account_payment_sale_id`) REFERENCES `tbl_pos_sales` (`sale_id`);

--
-- Constraints for table `tbl_pos_suppliers`
--
ALTER TABLE `tbl_pos_suppliers`
  ADD CONSTRAINT `tbl_pos_suppliers_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `tbl_pos_people` (`person_id`),
  ADD CONSTRAINT `tbl_pos_suppliers_ibfk_2` FOREIGN KEY (`tax_class_id`) REFERENCES `tbl_pos_tax_classes` (`id`);

--
-- Constraints for table `tbl_pos_suppliers_taxes`
--
ALTER TABLE `tbl_pos_suppliers_taxes`
  ADD CONSTRAINT `tbl_pos_suppliers_taxes_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `tbl_pos_suppliers` (`person_id`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_pos_supplier_store_accounts`
--
ALTER TABLE `tbl_pos_supplier_store_accounts`
  ADD CONSTRAINT `tbl_pos_supplier_store_accounts_ibfk_1` FOREIGN KEY (`receiving_id`) REFERENCES `tbl_pos_receivings` (`receiving_id`),
  ADD CONSTRAINT `tbl_pos_supplier_store_accounts_ibfk_2` FOREIGN KEY (`supplier_id`) REFERENCES `tbl_pos_suppliers` (`person_id`);

--
-- Constraints for table `tbl_pos_supplier_store_accounts_paid_receivings`
--
ALTER TABLE `tbl_pos_supplier_store_accounts_paid_receivings`
  ADD CONSTRAINT `tbl_pos_supplier_store_accounts_paid_receivings_ibfk_1` FOREIGN KEY (`receiving_id`) REFERENCES `tbl_pos_receivings` (`receiving_id`),
  ADD CONSTRAINT `tbl_pos_supplier_store_accounts_paid_receivings_ibfk_2` FOREIGN KEY (`store_account_payment_receiving_id`) REFERENCES `tbl_pos_receivings` (`receiving_id`);

--
-- Constraints for table `tbl_pos_tax_classes`
--
ALTER TABLE `tbl_pos_tax_classes`
  ADD CONSTRAINT `tbl_pos_tax_classes_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `tbl_pos_locations` (`location_id`);

--
-- Constraints for table `tbl_pos_tax_classes_taxes`
--
ALTER TABLE `tbl_pos_tax_classes_taxes`
  ADD CONSTRAINT `tbl_pos_tax_classes_taxes_ibfk_1` FOREIGN KEY (`tax_class_id`) REFERENCES `tbl_pos_tax_classes` (`id`);

--
-- Constraints for table `tbl_pos_zips`
--
ALTER TABLE `tbl_pos_zips`
  ADD CONSTRAINT `tbl_pos_zips_ibfk_1` FOREIGN KEY (`shipping_zone_id`) REFERENCES `tbl_pos_shipping_zones` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
